import React from "react";

import { Image } from "react-bootstrap";
import load from "./assets/loading.svg";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

const loading = (props) => (
  <div style={{ textAlign: "center" }}>
    <Image src={load} />
  </div>
);

// Pages
const Home = React.lazy(() => import("./views/pages/Home"));
const LandingPage = React.lazy(() => import("./views/pages/Landing"));
const History = React.lazy(() => import("./views/pages/History"));
const Print = React.lazy(() => import("./views/pages/Print"));
const Data = React.lazy(() => import("./views/pages/Daftar"));

function App(props) {
  return (
    <Router>
      <React.Suspense fallback={loading()}>
        <Switch>
          <Route path="/data">
            <Data />
          </Route>
          <Route path="/history">
            <History />
          </Route>
          <Route path="/print/:id">
            <Print />
          </Route>
          <Route path="/home">
            <Home />
          </Route>
          <Route path="/">
            <LandingPage />
          </Route>
        </Switch>
        {/* {token ? <Redirect to="/home" /> : null} */}
      </React.Suspense>
    </Router>
  );
}

export default App;
