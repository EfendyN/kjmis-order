import { USER_LOGIN } from "../../config/constants";

// Setup Reducer for Redux
const initialState = {
  user: null,
  loading: false,
  error: false,
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case `${USER_LOGIN}_PENDING`:
      return {
        ...state,
        loading: true,
      };

    case `${USER_LOGIN}_FULFILLED`:
      return {
        ...state,
        user: action.payload,
        loading: false,
      };

    case `${USER_LOGIN}_REJECTED`:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default user;
