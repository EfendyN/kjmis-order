import { LOGIN } from "../../config/constants";

// Setup Reducer for Redux
const initialState = {
  data: [],
  auth: false,
  loading: false,
  error: false,
};

const login = (state = initialState, action) => {
  switch (action.type) {
    case `${LOGIN}_PENDING`:
      return {
        ...state,
        auth: false,
        loading: true,
      };

    case `${LOGIN}_FULFILLED`:
      return {
        ...state,
        auth: true,
        data: action.payload,
        loading: false,
      };
    case `${LOGIN}_REJECTED`:
      return {
        ...state,
        auth: false,
        loading: false,
        error: true,
      };
    default:
      return state;
  }
};

export default login;
