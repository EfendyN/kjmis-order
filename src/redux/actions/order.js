import { API } from "../../config/api";

export const putOrder = (id, dataOrder) => {
  return {
    type: "ORDER_EDIT",
    payload: async () => {
      const res = await API.put(`/order?id=${id}`, { dataOrder });
      const { data } = res.data;
      return data;
    },
  };
};

export const deleteOrder = (id, password) => {
  return {
    type: "DELETE_ORDER",
    payload: async () => {
      const res = await API.delete(
        `/order?order_id=${id}&password=${password}`
      );
      const { data } = res.data;
      return data;
    },
  };
};

export const updateRincian = (id, dataRincian) => {
  return {
    type: "UPDATE_RINCIAN",
    payload: async () => {
      const res = await API.put(`/rincian?id=${id}`, { dataRincian });
      const { data } = res.data;
      return data;
    },
  };
};

export const createRincian = (id, dataRincian) => {
  return {
    type: "CREATE_RINCIAN",
    payload: async () => {
      const res = await API.post(`/rincians?order_id=${id}`, { dataRincian });
      const { data } = res.data;
      return data;
    },
  };
};
export const deleteRincian = (id) => {
  return {
    type: "DELETE_RINCIAN",
    payload: async () => {
      const res = await API.delete(`/rincian?id=${id}`);
      const { data } = res.data;
      return data;
    },
  };
};
