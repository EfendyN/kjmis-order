import { API } from "../../config/api";
import {
  LIST_INDEX,
  GET_RINCIAN,
  ADD_PESANAN,
  GET_PROVINSI,
  GET_KOTA,
} from "../../config/constants";

export const listIndex = (values) => {
  const {
    asal_pesanan,
    tgl_surat_pesanan,
    no_surat_pesanan,
    tujuan_provinsi,
    no_surat_tujuan,
    tgl_surat_tujuan,
    daerah_tujuan,
    tgl_pengambilan,
    status,
    tujuan_supplier,
  } = values;
  return {
    type: LIST_INDEX,
    payload: async () => {
      const res = await API.post(
        `/orders?asal_pesanan=${asal_pesanan}&tgl_surat_pesanan=${tgl_surat_pesanan}&no_surat_pesanan=${no_surat_pesanan}&tujuan_provinsi=${tujuan_provinsi}&tujuan_supplier=${tujuan_supplier}&no_surat_tujuan=${no_surat_tujuan}&tgl_surat_tujuan=${tgl_surat_tujuan}&daerah_tujuan=${daerah_tujuan}&tgl_pengambilan=${tgl_pengambilan}&status=${status}`
      );
      const { data } = res.data;
      return data;
    },
  };
};

export const getDetail = (id) => {
  return {
    type: "GET_DETAIL_SURAT",
    payload: async () => {
      const res = await API.get(`/order?order_id=${id}`);
      const { data } = res.data;
      return data;
    },
  };
};

export const getRincianOrder = (id) => {
  return {
    type: GET_RINCIAN,
    payload: async () => {
      const res = await API.get(`/order?order_id=${id}`);
      const { data } = res.data;
      return data;
    },
  };
};

export const addPesanan = (order, rincian) => {
  return {
    type: ADD_PESANAN,
    payload: async () => {
      // console.log({ order, rincian });
      const list_rincian = rincian;
      const res = await API.post(`/order`, {
        order,
        list_rincian,
      });
      const { data } = res.data;
      return data;
    },
  };
};

export const provinsiIndex = () => {
  return {
    type: GET_PROVINSI,
    payload: async () => {
      const res = await API.get(`/provinsi`);
      const { data } = res.data;
      return data;
    },
  };
};

export const kotaIndex = (prov) => {
  return {
    type: GET_KOTA,
    payload: async () => {
      const res = await API.get(`/kotakab?x=${prov}`);
      const { data } = res.data;

      return data;
    },
  };
};

export const kotaBulogIndex = () => {
  return {
    type: "GET_BULOG",
    payload: async () => {
      const res = await API.get(`/bulog`);
      const { data } = res.data;
      return data;
    },
  };
};

export const barangIndex = () => {
  return {
    type: "GET_BARANG",
    payload: async () => {
      const res = await API.get(`/barang`);
      const { data } = res.data;
      return data;
    },
  };
};

export const getTipeUnit = (unit) => {
  return {
    type: "GET_UNIT",
    payload: async () => {
      const res = await API.get(`/unit?x=${unit}`);
      const { data } = res.data;
      return data;
    },
  };
};

export const getHetBarang = (b, p) => {
  return {
    type: "GET_HET",
    payload: async () => {
      const res = await API.get(`/het?b=${b}&p=${p}`);
      const { data } = res.data;
      return data;
    },
  };
};

export const paymentUpdate = (id, pay, type) => {
  return {
    type: "PUT_PAYMENT",

    payload: async () => {
      const res = await API.put(`/payment?payment_id=${id}&type=${type}`, {
        pay,
      });
      const { data } = res.data;
      return data;
    },
  };
};
