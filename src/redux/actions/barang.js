import { API } from "../../config/api";

export const getHetIndex = (values) => {
  return {
    type: "HET_INDEX",
    payload: async () => {
      const { nama_barang, wilayah, status } = values;
      const res = await API.get(
        `/hets?nama=${nama_barang}&wilayah=${wilayah}&status=${status}`
      );
      const { data } = res.data;
      return data;
    },
  };
};

export const postHet = (dataHet) => {
  return {
    type: "POST_HET",
    payload: async () => {
      // console.log({ dataHet });
      const res = await API.post(`/het`, { dataHet });
      const { data } = res.data;
      return data;
    },
  };
};

export const getBarangIndex = () => {
  return {
    type: "BARANG_INDEX",
    payload: async () => {
      const res = await API.get(`/barang`);
      const { data } = res.data;
      return data;
    },
  };
};

export const postBarang = (dataBarang) => {
  return {
    type: "POST_BARANG",
    payload: async () => {
      const res = await API.post(`/barang`, { dataBarang });
      const { data } = res.data;
      return data;
    },
  };
};

export const deleteBarang = (id) => {
  return {
    type: "DELETE_BARANG",
    payload: async () => {
      const res = await API.delete(`/barang?id=${id}`);
      const { data } = res.data;
      return data;
    },
  };
};

