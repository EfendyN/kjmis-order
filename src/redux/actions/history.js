import { API } from "../../config/api";

export const getHasil = (values) => {
  return {
    type: "HASIL",
    payload: async () => {
      // const { nama_barang, dari_tgl, sampai_tgl, provinsi, daerah } = values;

      const res = await API.post(`/history`, { values });
      const { data } = res.data;
      // console.log({ data });
      return data;
    },
  };
};
