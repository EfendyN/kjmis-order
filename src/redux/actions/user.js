import { USER_LOGIN } from "../../config/constants";
import { API, setHeaderAuth } from "../../config/api";

export const getUser = () => {
  return {
    type: USER_LOGIN,
    payload: async () => {
      const token = localStorage.getItem("token");
      setHeaderAuth(token);
      const res = await API.get("/user");
      const { data } = res.data;
      return data;
    },
  };
};
