import { LOGIN } from "../../config/constants";
import { API } from "../../config/api";

export const login = (username, password) => {
  return {
    type: LOGIN,
    payload: async () => {
      const res = await API.post("/login", {
        username,
        password,
      });
      const data = res.data;
      if (data.message === "success login") {
        localStorage.setItem("token", data.token);
        return data;
      } else {
        return data;
      }
    },
  };
};

export const fetchUpdatePassword = (id, dataUpdate) => {
  return {
    type: "UPDATE_PASSWORD",
    payload: async () => {
      // console.log(dataUpdate);
      const res = await API.put(`/user?id=${id}`, { dataUpdate });
      const { message } = res.data;
      return message;
    },
  };
};
