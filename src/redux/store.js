import { createStore, combineReducers, applyMiddleware } from "redux";

import list from "./reducers/list";
import login from "./reducers/auth";
import user from "./reducers/user";

import logger from "redux-logger";
import promise from "redux-promise-middleware";

const middleware = [promise];
// Global state
const rootReducers = combineReducers({
  list,
  login,
  user,
});

// Setup store for Redux
if (process.env.NODE_ENV === "development") {
  // add `redux-logger`
  middleware.push(logger);
}
const store = createStore(rootReducers, applyMiddleware(...middleware));

export default store;
