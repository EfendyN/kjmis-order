import React, { useState } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { Helmet } from "react-helmet";
import { Row, Col, Card, CardBody } from "reactstrap";

import DataTable from "../../organisms/DataTable/PesananDataTable";
import ModalDetail from "../../organisms/ModalDetail";
import ModalTambahPemesanan from "../../organisms/ModalTambahPemesanan";
import ModalPayment from "../../organisms/ModalPayment";
import NavBarKJMIS from "../../components/NavBar";

const Home = (props) => {
  const [modalDetailIsOpen, setModalDetailIsOpen] = useState(false);
  const [activeData, setActiveData] = useState(null);

  const [modalTambahPemesanan, setModalTambahPemesanan] = useState(null);
  const [modalPayment, setModalPayment] = useState(null);

  const [next, setNext] = useState(false);

  const toggleModalTambahPemesanan = () => {
    setModalTambahPemesanan(!modalTambahPemesanan);
  };

  const toggleModalPayment = () => {
    if (modalPayment) {
      setActiveData(null);
    }
    setModalPayment(!modalPayment);
    setNext(!next);
  };

  const toggleModalDetail = () => {
    if (modalDetailIsOpen) {
      setActiveData(null);
    }
    setModalDetailIsOpen(!modalDetailIsOpen);
  };

  const handleClikDetail = (data) => {
    setActiveData(data);
    toggleModalDetail();
  };
  const handlePayment = (data) => {
    setActiveData(data);
    toggleModalPayment();
  };

  const token = localStorage.getItem("token");
  return token ? (
    <div className="animated fadeIn">
      <Helmet>
        <title>KJMIS</title>
      </Helmet>
      <NavBarKJMIS
        mode="DATA PESANAN"
        toggleModalTambahPemesanan={toggleModalTambahPemesanan}
      />
      <Row>
        <Col xs="12" lg="12">
          <Card>
            <CardBody>
              <DataTable
                handleClikDetail={handleClikDetail}
                handleClikPayment={handlePayment}
                modalDetailIsOpen={modalDetailIsOpen}
                modalPayment={modalPayment}
              />
            </CardBody>
          </Card>
        </Col>
      </Row>

      <ModalDetail
        isOpen={modalDetailIsOpen}
        toggle={toggleModalDetail}
        activeData={activeData}
      />

      <ModalTambahPemesanan
        isOpen={modalTambahPemesanan}
        toggle={toggleModalTambahPemesanan}
      />

      <ModalPayment
        isOpen={modalPayment}
        next={next}
        toggle={toggleModalPayment}
        data={activeData}
      />
    </div>
  ) : (
    <>
      <Redirect to="/"></Redirect>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};

export default connect(mapStateToProps, null)(Home);
