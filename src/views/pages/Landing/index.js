import { connect } from "react-redux";
import { login } from "../../../redux/actions/auth";
import React, { useState } from "react";
import { Button, Modal, Form, Row, Col, Image } from "react-bootstrap";

import { Alert, FormFeedback, Input } from "reactstrap";

import { Helmet } from "react-helmet";

import { Redirect } from "react-router-dom";

import kjmis from "../../components/image/kjmis.jpeg";
import sjs from "../../components/image/sjs.jpeg";

import image from "../../components/image/undraw/landingimage.svg";
import paper from "../../components/image/undraw/paper.svg";

import "./landing.css";
import { getUser } from "../../../redux/actions/user";

function Landing(props) {
  const [show, setShow] = useState(false);
  const [alert, setAlert] = useState({
    wrong: false,
    success: false,
    invalidpass: false,
    invaliduser: false,
  });

  const [login, setLogin] = useState({
    username: "",
    password: "",
  });

  const handleLogin = (e) => {
    setLogin({ ...login, [e.target.name]: e.target.value });
    setAlert({
      wrong: false,
      invalidpass: false,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (login.password !== "" && login.username !== "") {
      const login2 = await props.login(login.username, login.password);

      if (login2.action.payload.message === "Username atau Password Salah") {
        setAlert({ wrong: true });
        setLogin({ username: "", password: "" });
      } else if (login2.action.payload.message === "Password Salah") {
        setAlert({ wrong: true });
        setLogin({ username: "", password: "" });
      }
      if (login2.action.payload.message === "success login") {
        setAlert({ succes: true });
        window.location.reload();
      }
    } else if (login.username === "") {
      setAlert({ invaliduser: true });
    } else if (login.password === "") {
      setAlert({ invalidpass: true });
    }
  };

  const token = localStorage.getItem("token");

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return token ? (
    <Redirect to="/home" />
  ) : (
    <div>
      <Helmet>
        <title>KJMIS</title>
      </Helmet>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>
        <Form onSubmit={handleSubmit}>
          <Modal.Body>
            {alert.wrong ? (
              <Alert color="danger">Username/Password Salah</Alert>
            ) : null}
            {alert.success ? (
              <Alert color="success">Berhasil Masuk</Alert>
            ) : null}

            <Form.Group controlId="formGroupEmail">
              <Form.Label>Username</Form.Label>
              <Input
                name="username"
                invalid={alert.invaliduser}
                value={login.username}
                onChange={(e) => handleLogin(e)}
                type="text"
                placeholder="Masukkan Username"
              />
              <FormFeedback>Username Tidak Boleh Kosong</FormFeedback>
            </Form.Group>
            <Form.Group controlId="formGroupPassword">
              <Form.Label>Password</Form.Label>
              <Input
                invalid={alert.invalidpass}
                name="password"
                value={login.password}
                onChange={(e) => handleLogin(e)}
                type="password"
                placeholder="Masukkan Password"
              />
              <FormFeedback>Password Tidak Boleh Kosong</FormFeedback>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Batal
            </Button>
            {token ? (
              <>
                <Redirect to="/home" />
              </>
            ) : (
              <>
                <Button
                  variant="primary"
                  onClick={(e) => handleSubmit(e)}
                  type="submit"
                >
                  Masuk
                </Button>
              </>
            )}
          </Modal.Footer>
        </Form>
      </Modal>
      <div>
        <Image className="paper" src={paper}></Image>
        <Image className="kjmis-icon" src={kjmis}></Image>
        <Image className="sjs-icon" src={sjs}></Image>
        <Row className="row">
          <Col className="col-1" md="6" xs="12">
            <h1>Selamat Datang</h1>

            <h3>point of sales</h3>
            <Button
              className="button"
              size="lg"
              variant="primary"
              onClick={handleShow}
            >
              Login
            </Button>
          </Col>
          <Col className="col-2" md="6" xs="12">
            <Image className="image" src={image}></Image>
          </Col>
        </Row>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    Login: state.login,
    User: state.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (username, password) => dispatch(login(username, password)),
    getUser: () => dispatch(getUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
