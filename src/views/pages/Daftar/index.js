import React, { useState } from "react";
import { Tab, Row, Nav, Col, Card } from "react-bootstrap";
import NavBarKJMIS from "../../components/NavBar";
import BarangDataTable from "../../organisms/DataTable/BarangDataTable";
import HetDataTable from "../../organisms/DataTable/HetDataTable";
import ModalTambahBarang from "../../organisms/ModalTambahBarang";
import { Button } from "reactstrap";
import { Redirect } from "react-router-dom";
const Daftar = () => {
  const [modalTambahBarang, setModalTambahBarang] = useState(false);
  const [modalTambahHET, setModalTambahHET] = useState(false);
  const [dataBarangs, setDataBarangs] = useState(false);
  const [dataWilayah, setDataWilayah] = useState(false);

  const togleModalTambahBarang = () => {
    setModalTambahBarang(!modalTambahBarang);
  };

  const togleModalTambahHET = () => {
    setModalTambahHET(!modalTambahHET);
  };
  const token = localStorage.getItem("token");
  return token ? (
    <>
      <NavBarKJMIS mode="DATA" />
      <Card>
        <Card.Body>
          <Tab.Container id="left-tabs-example" defaultActiveKey="first">
            <Row>
              <Col sm={2}>
                <Nav variant="pills" className="flex-column">
                  <Nav.Item>
                    <Nav.Link eventKey="first">DATA BARANG</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="second">DATA HET</Nav.Link>
                  </Nav.Item>
                </Nav>
              </Col>
              <Col sm={10}>
                <Tab.Content>
                  <Tab.Pane eventKey="first">
                    <Card>
                      <Card.Header>
                        <strong>Data Barang</strong>{" "}
                        <Button
                          size="sm"
                          outline
                          color="primary"
                          onClick={togleModalTambahBarang}
                        >
                          Tambah Barang
                        </Button>
                      </Card.Header>
                      <Card.Body>
                        <BarangDataTable modalTambah={modalTambahBarang} />
                      </Card.Body>
                    </Card>
                  </Tab.Pane>
                  <Tab.Pane eventKey="second">
                    <Card>
                      <Card.Header>
                        <strong>Data HET</strong>{" "}
                        <Button
                          size="sm"
                          outline
                          color="primary"
                          onClick={togleModalTambahHET}
                        >
                          Tambah HET
                        </Button>
                      </Card.Header>
                      <Card.Body>
                        <HetDataTable
                          dataBarangs={dataBarangs}
                          dataWilayah={dataWilayah}
                          setDataWilayah={setDataWilayah}
                          setDataBarangs={setDataBarangs}
                          modalTambahHET={modalTambahHET}
                          togleModalTambahHET={togleModalTambahHET}
                        />
                      </Card.Body>
                    </Card>
                  </Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
        </Card.Body>
      </Card>

      <ModalTambahBarang
        isOpen={modalTambahBarang}
        toggle={togleModalTambahBarang}
      />
    </>
  ) : (
    <Redirect to="/" />
  );
};
export default Daftar;
