import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { Helmet } from "react-helmet";
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Button,
  Alert,
  Label,
} from "reactstrap";

import { Form, Image } from "react-bootstrap";

import DataTable from "../../organisms/DataTable/RiwayatDataTable";
import ModalDetail from "../../organisms/ModalDetail";
import ModalPayment from "../../organisms/ModalPayment";
// import Hasil from "../../components/Hasil";

import NavBarKJMIS from "../../components/NavBar";

import {
  getTipeUnit,
  barangIndex,
  kotaBulogIndex,
  kotaIndex,
} from "../../../redux/actions/list";

import { getHasil } from "../../../redux/actions/history";
import plus from "../../../assets/plus.svg";

const History = ({
  getTipeUnit,
  barangIndex,
  kotaBulogIndex,
  getHasil,
  user,
  kotaIndex,
}) => {
  const [modalDetailIsOpen, setModalDetailIsOpen] = useState(false);
  const [activeData, setActiveData] = useState(null);
  const [modalPayment, setModalPayment] = useState(null);
  const [dataTable, setDataTable] = useState({
    data: [],
    total_penerimaan: null,
    total_pembayaran: null,
    total_unit: null,
  });

  const [alert, setAlert] = useState(false);
  const [hasil, setHasil] = useState(false);
  const [xhasil, setXhasil] = useState(false);

  const [barang, setBarang] = useState([]);
  const [provinsi, setProvinsi] = useState([]);
  const [kota, setKota] = useState([]);
  const [values, setValues] = useState({
    barang: "",
    provinsi: "",
    daerah: "",
  });
  const [content, setContent] = useState({
    barang: [],
    provinsi: [],
    daerah: [],
    from: "",
    to: "",
  });
  const [next, setNext] = useState(false);

  useEffect(() => {
    const dataKotaBulog = async () => {
      const res = await kotaBulogIndex();
      setProvinsi(res.action.payload);
    };
    const dataBarang = async () => {
      const res = await barangIndex();
      setBarang(res.action.payload);
    };
    const dataKota = async () => {
      const res = await kotaIndex(values.provinsi);
      setKota([res.action.payload.provinsi]);
    };
    dataKota();
    dataBarang();
    dataKotaBulog();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const toggleModalPayment = () => {
    if (modalPayment) {
      setActiveData(null);
    }
    setModalPayment(!modalPayment);
    setNext(!next);
  };

  useEffect(() => {
    const dataKota = async () => {
      const res = await kotaIndex(values.provinsi);
      setKota(res.action.payload.provinsi);
      // console.log(res);
    };
    dataKota();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [values.provinsi]);

  useEffect(() => {
    const getunit = async () => {
      const nama = values.barang;
      const res = await getTipeUnit(nama);
      if (nama === null) {
        return null;
      } else {
        setValues({
          ...values,
          tipe_unit: res.action.payload?.tipe_unit,
        });
      }
    };
    if (values.barang !== "") getunit();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [values.barang]);

  const toggleModalDetail = () => {
    if (modalDetailIsOpen) {
      setActiveData(null);
    }
    setModalDetailIsOpen(!modalDetailIsOpen);
  };

  const handleClikDetail = (data) => {
    setActiveData(data);
    toggleModalDetail();
  };
  const handlePayment = (data) => {
    setActiveData(data);
    toggleModalPayment();
  };
  const handleChange = (e) => {
    setAlert(false);
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
    setHasil(false);
  };

  const handleSearch = async (e) => {
    e.preventDefault();
    if (values.barang !== "" && values.from !== "" && values.to !== "") {
      const x = await getHasil(content);
      if (x) {
        // console.log({ x });
        setDataTable({
          data: x.action.payload.order,
        });
        if (x.action.payload.message === "Data Ditemukan") {
          setHasil(true);
          setXhasil(false);
          window.scrollTo(0, 500);
        } else {
          setHasil(false);
          setXhasil(true);
        }
      }
    } else {
      setAlert(true);
    }
  };

  const addArray = (key) => {
    const result = content[key].filter((word) => word === values[key]);
    if (values[key] !== "" && result.length <= 0)
      setContent({
        ...content,
        [key]: [...content[key], values[key]],
      });
  };

  const deleteArray = (key, obj) => {
    const filteredItems = content[obj].filter((item) => item !== key);
    setContent({ ...content, [obj]: filteredItems });
  };

  const token = localStorage.getItem("token");

  return token ? (
    <div className="animated fadeIn">
      <Helmet>
        <title>KJMIS</title>
      </Helmet>
      <NavBarKJMIS mode="RIWAYAT PESANAN" />
      <Row>
        <Col xs="12" lg="12">
          <Card>
            <CardBody>
              <Card>
                <CardBody>
                  <Card>
                    <CardBody style={{ marginBottom: 14 }}>
                      <Row>
                        <Col lg={4}>
                          {" "}
                          <Form>
                            <Label>Pilih Barang</Label>
                            <Form.Group as={Row}>
                              <Col sm={8}>
                                <Form.Control
                                  as="select"
                                  name="barang"
                                  value={values.barang}
                                  onChange={(e) => handleChange(e)}
                                >
                                  <option value="">Pilih Barang</option>
                                  {barang.map((item, index) => (
                                    <option value={item.nama} key={index}>
                                      {item.nama}
                                    </option>
                                  ))}
                                </Form.Control>
                              </Col>
                              <Col sm={2}>
                                <Image
                                  src={plus}
                                  style={{
                                    cursor: "pointer",
                                    width: 35,
                                    height: 35,
                                  }}
                                  onClick={() => addArray("barang")}
                                />
                              </Col>
                            </Form.Group>
                            <br />
                            <Label>Pilih Provinsi</Label>
                            <Form.Group as={Row}>
                              <Col sm={8}>
                                <Form.Control
                                  as="select"
                                  name="provinsi"
                                  value={values.provinsi}
                                  onChange={(e) => handleChange(e)}
                                >
                                  <option value="">Pilih Barang</option>
                                  {provinsi.map((item, index) => (
                                    <option value={item.provinsi} key={index}>
                                      {item.provinsi}
                                    </option>
                                  ))}
                                </Form.Control>
                              </Col>
                              <Col sm={2}>
                                <Image
                                  src={plus}
                                  style={{
                                    cursor: "pointer",
                                    width: 35,
                                    height: 35,
                                  }}
                                  onClick={() => addArray("provinsi")}
                                />
                              </Col>
                            </Form.Group>{" "}
                            <br />
                            {values.provinsi !== "" ? (
                              <>
                                <Label>Pilih Daerah</Label>
                                <Form.Group as={Row}>
                                  <Col sm={8}>
                                    <Form.Control
                                      as="select"
                                      name="daerah"
                                      value={values.daerah}
                                      onChange={(e) => handleChange(e)}
                                    >
                                      <option value="">Pilih Barang</option>
                                      {kota.map((item, index) => (
                                        <option key={index}>
                                          {item.wil_kab}
                                        </option>
                                      ))}
                                    </Form.Control>
                                  </Col>
                                  <Col sm={2}>
                                    <Image
                                      src={plus}
                                      style={{
                                        cursor: "pointer",
                                        width: 35,
                                        height: 35,
                                      }}
                                      onClick={() => addArray("daerah")}
                                    />
                                  </Col>
                                </Form.Group>
                              </>
                            ) : null}
                            <Form.Group as={Row}>
                              <Form.Label column sm={5}>
                                Dari Tanggal
                              </Form.Label>
                              <Col sm={7}>
                                <Form.Control
                                  type="date"
                                  name="from"
                                  // value={values.from}
                                  onChange={(e) => {
                                    handleChange(e);
                                    setContent({
                                      ...content,
                                      from: e.target.value,
                                    });
                                  }}
                                />
                              </Col>
                            </Form.Group>
                            <Form.Group as={Row}>
                              <Form.Label column sm={5}>
                                Sampai Tanggal
                              </Form.Label>
                              <Col sm={7}>
                                <Form.Control
                                  type="date"
                                  name="to"
                                  // value={values.to}
                                  onChange={(e) => {
                                    handleChange(e);
                                    setContent({
                                      ...content,
                                      to: e.target.value,
                                    });
                                  }}
                                />
                              </Col>
                            </Form.Group>
                            {alert ? (
                              <Alert color="danger">
                                Harap isi Tanggal/Nama Barang
                              </Alert>
                            ) : null}
                          </Form>
                        </Col>
                        <Col lg={6}>
                          <Label>Barang :</Label>
                          <br />
                          {content.barang.map((item, index) => (
                            <>
                              <Button
                                key={index}
                                size="sm"
                                outline
                                color="primary"
                                style={{
                                  fontSize: "14px",
                                }}
                                onClick={() => deleteArray(item, "barang")}
                              >
                                {item}
                              </Button>{" "}
                            </>
                          ))}
                          <br />
                          <br />
                          <Label>Provinsi :</Label>
                          <br />
                          {content.provinsi.map((item, index) => (
                            <>
                              <Button
                                key={index}
                                size="sm"
                                outline
                                color="primary"
                                style={{
                                  fontSize: "14px",
                                }}
                                onClick={() => deleteArray(item, "provinsi")}
                              >
                                {item}
                              </Button>{" "}
                            </>
                          ))}
                          <br />
                          <br />
                          <Label>Daerah :</Label>
                          <br />
                          {content.daerah.map((item, index) => (
                            <>
                              <Button
                                key={index}
                                size="sm"
                                outline
                                color="primary"
                                style={{
                                  fontSize: "14px",
                                }}
                                onClick={(key) => deleteArray(item, "daerah")}
                              >
                                {item}
                              </Button>{" "}
                            </>
                          ))}
                          <br />
                        </Col>
                      </Row>
                      <div style={{ justifyContent: "end" }}>
                        <Button
                          outline
                          color="primary"
                          onClick={(e) => handleSearch(e)}
                          className="pl-5 pr-5 pt-2 pb-2"
                        >
                          Cari
                        </Button>
                      </div>

                      <br />
                    </CardBody>
                    <CardBody>
                      {xhasil ? (
                        <Alert color="danger">Data Tidak Ditemukan</Alert>
                      ) : null}
                    </CardBody>
                  </Card>
                  <br />

                  {hasil ? (
                    <DataTable
                      id="table-data"
                      handleClikDetail={handleClikDetail}
                      handleClikPayment={handlePayment}
                      dataTable={dataTable}
                    />
                  ) : null}
                </CardBody>
              </Card>
              <br />
            </CardBody>
          </Card>
        </Col>
      </Row>

      <ModalDetail
        isOpen={modalDetailIsOpen}
        toggle={toggleModalDetail}
        activeData={activeData}
      />

      <ModalPayment
        isOpen={modalPayment}
        next={next}
        toggle={toggleModalPayment}
        data={activeData}
      />
    </div>
  ) : (
    <>
      <Redirect to="/"></Redirect>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    kotaBulogIndex: () => dispatch(kotaBulogIndex()),
    barangIndex: () => dispatch(barangIndex()),
    getTipeUnit: (unit) => dispatch(getTipeUnit(unit)),
    getHasil: (values) => dispatch(getHasil(values)),
    kotaIndex: (provinsi) => dispatch(kotaIndex(provinsi)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(History);
