import React, { useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Label,
  Input,
  FormFeedback,
  FormGroup,
  Alert,
  ModalFooter,
  Button,
} from "reactstrap";

import { deleteOrder } from "../../../redux/actions/order";
import { connect } from "react-redux";

const ModalDelete = ({ isOpen, toggle, user, deleteOrder, data }) => {
  const [password, setPassword] = useState("");
  const [modalSuccess, setModalSuccess] = useState(false);
  const [alert, setAlert] = useState({
    password: false,
    success: false,
  });
  const handleChange = (e) => {
    setPassword(e.target.value);
  };
  const handleSubmit = async () => {
    const x = await deleteOrder(data, password);
    if (x) {
      // console.log(x.action.payload, "xxxxxxxxxxx");
      if (x.action.payload.message === "Password Salah") {
        setAlert({ ...alert, password: true, success: false });
      } else {
        setAlert({ ...alert, password: false, success: true });
        setModalSuccess(!modalSuccess);
      }
    }
  };
  return (
    <>
      <Modal size="sm" isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>Hapus Pesanan ?</ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label>Masukkan Password</Label>

            <Input
              name="password"
              invalid={alert.password}
              placeholder="Masukkan Password"
              type="password"
              value={password}
              onChange={(e) => handleChange(e)}
            />
            <FormFeedback>Password Salah</FormFeedback>
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color="success" onClick={toggle}>
            Batal
          </Button>
          <Button color="danger" onClick={() => handleSubmit()}>
            Hapus Pesanan
          </Button>
        </ModalFooter>
      </Modal>

      <Modal isOpen={modalSuccess} toggle={() => window.location.reload()}>
        {/* <ModalHeader toggle={() => window.location.reload()}></ModalHeader> */}
        <ModalBody>
          {alert.success ? (
            <Alert color="success">Pesanan Berhasil Dihapus</Alert>
          ) : null}
        </ModalBody>
      </Modal>
    </>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    deleteOrder: (id, password) => dispatch(deleteOrder(id, password)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ModalDelete);
