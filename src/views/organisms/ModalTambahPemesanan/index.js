import React, { useState, useEffect } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Col,
  Button,
  FormGroup,
  Label,
  Input,
  Card,
  CardBody,
  CardHeader,
  Alert,
} from "reactstrap";
import { Form, Image } from "react-bootstrap";
// import rincianBarang from "../../../constans/imputRincian";
import { connect } from "react-redux";
import {
  addPesanan,
  provinsiIndex,
  kotaIndex,
  kotaBulogIndex,
  barangIndex,
  getTipeUnit,
  getHetBarang,
} from "../../../redux/actions/list";
import moment from "moment";
import FormRincianBarang from "../../components/FormRincianBarang";
import loading from "../../../assets/loading.svg";

const TambahPesananModal = ({
  isOpen,
  toggle,
  addPesanan,
  kotaIndex,
  kotaBulogIndex,
  barangIndex,
  getTipeUnit,
  getHetBarang,
}) => {
  const [next, setNext] = useState(false);
  const [btext, setbtext] = useState("Rincian Pesanan");
  const [tittle, setTittle] = useState("Pesanan");
  const [barang, setBarang] = useState({ data: [] });
  const [alert, setAlert] = useState(false);
  const [alertR, setAlertR] = useState(false);
  const [submit, setSubmit] = useState(false);
  const [kota, setKota] = useState({ data: [] });
  const [kotaBulog, setKotaBulog] = useState({ data: [] });
  const [provinsi, setProvinsi] = useState("");
  const [rincian, setRincian] = useState([]);
  const [buttonLoading, setButtonLoading] = useState(false);

  const [minTglTujuan, setMinTglTujuan] = useState("");

  const [hetInput, setHetInput] = useState(true);

  const [supplier, setSupplier] = useState(false);
  const [forms, setForms] = useState(false);

  const [order, setOrder] = useState({
    asal_pesanan: "",
    tgl_surat_pesanan: "",
    no_surat_pesanan: "",
    tujuan_provinsi: "",
    no_surat_tujuan: "",
    tgl_surat_tujuan: "",
    daerah_tujuan: "",
    daerah_tujuan_bulog: "",
    tgl_pengambilan: "",
    tujuan_supplier: "",
  });

  const [valueRincian, setValueRincian] = useState({
    key: "",
    nama_barang: "",
    unit: "",
    tipe_unit: "",
    jumlah: "",
    harga_perunit: "",
    total_harga: "",
    het: "",
    total_het: "",
  });

  useEffect(() => {
    const dataKota = async () => {
      const res = await kotaIndex(provinsi);
      setKota({ data: res.action.payload.provinsi });
    };
    const dataKotaBulog = async () => {
      const res = await kotaBulogIndex();
      setKotaBulog({ data: res.action.payload });
    };
    const dataBarang = async () => {
      const res = await barangIndex();
      setBarang({ data: res.action.payload });
    };
    if (isOpen) {
      dataBarang();
      dataKota();
      dataKotaBulog();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isOpen]);

  useEffect(() => {
    const dataKota = async () => {
      const res = await kotaIndex(provinsi);
      setKota({ data: res.action.payload.provinsi });
    };
    dataKota();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [provinsi]);

  useEffect(() => {
    const tgl = async () => {
      setOrder({
        ...order,
        tgl_pengambilan: moment(order.tgl_surat_tujuan)
          .add(1, "day")
          .format("YYYY-MM-DD")
          .toString(),
      });
    };
    tgl();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [order.tgl_surat_tujuan]);

  useEffect(() => {
    const tgl = async () => {
      setMinTglTujuan(order.tgl_surat_pesanan);
    };
    tgl();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [order.tgl_surat_pesanan]);

  const handleChangeFormOrder = (e) => {
    setOrder({ ...order, [e.target.name]: e.target.value });
    setAlert(false);
  };

  const handleSetFormBulog = () => {
    setOrder({
      ...order,
      // asal_pesanan: "",
      // tgl_surat_pesanan: "",
      // no_surat_pesanan: "",
      tujuan_provinsi: "",
      no_surat_tujuan: "",
      tgl_surat_tujuan: "",
      daerah_tujuan: "",
      daerah_tujuan_bulog: "",
      tgl_pengambilan: "",
      tujuan_supplier: "BULOG",
    });
    setAlert(false);
    setProvinsi("");
  };

  const handleChangeNullFormOrder = () => {
    setOrder({
      ...order,
      // asal_pesanan: "",
      // tgl_surat_pesanan: "",
      // no_surat_pesanan: "",
      tujuan_provinsi: "",
      no_surat_tujuan: "",
      tgl_surat_tujuan: "",
      daerah_tujuan: "",
      daerah_tujuan_bulog: "",
      tgl_pengambilan: "",
      tujuan_supplier: "",
    });
    setAlert(false);
    setProvinsi("");
  };

  const handleChangeKota = async (e) => {
    await setProvinsi(e.target.value);
  };

  const handleSubmit = async () => {
    if (rincian.length < 1) {
      setAlertR(true);
    } else {
      await setSubmit(!submit);
    }
  };

  useEffect(() => {
    const handleSubmitx = async () => {
      setButtonLoading(true);
      const x = await addPesanan(order, rincian);
      if (x) {
        setButtonLoading(false);
        window.location.reload();
      }
    };
    if (submit) {
      handleSubmitx();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [submit]);

  const handleChangeRincian = (e) => {
    setValueRincian({
      ...valueRincian,
      [e.target.name]: e.target.value,
      key: Date.now(),
    });
  };

  const buttonTambah = async (e) => {
    if (valueRincian.harga_perunit !== "" && valueRincian.het !== "")
      setValueRincian({
        ...valueRincian,
        total_harga:
          valueRincian.jumlah * valueRincian.harga_perunit * valueRincian.unit,
        total_het: valueRincian.jumlah * valueRincian.het * valueRincian.unit,
      });

    setAlertR(false);
  };

  const deleteRincian = (key) => {
    const filteredItems = rincian.filter((item) => item.key !== key);
    setRincian(filteredItems);
  };

  useEffect(() => {
    if (
      valueRincian.key !== "" &&
      valueRincian.nama_barang !== "" &&
      valueRincian.unit !== "" &&
      valueRincian.jumlah !== "" &&
      valueRincian.total_harga !== "" &&
      valueRincian.total_het !== ""
    ) {
      setRincian([...rincian, valueRincian]);
      setValueRincian({
        key: "",
        nama_barang: "",
        unit: "",
        unit_type: "",
        jumlah: "",
        harga_perunit: "",
        total_harga: "",
        het: "",
        total_het: "",
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [valueRincian.total_harga]);

  useEffect(() => {
    const getunit = async () => {
      const nama = valueRincian.nama_barang;
      const p = order.tujuan_provinsi;
      const resHet = await getHetBarang(nama, p);
      const res = await getTipeUnit(nama);
      if (nama === null) {
        return null;
      } else {
        setValueRincian({
          ...valueRincian,
          tipe_unit: res.action.payload?.tipe_unit,
          het: resHet.action.payload?.het || "",
        });
        if (resHet.action.payload?.het) {
          setHetInput(false);
        } else {
          setHetInput(true);
        }
      }
    };
    getunit();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [valueRincian.nama_barang]);

  return (
    <Modal
      isOpen={isOpen}
      toggle={toggle}
      backdrop="static"
      size={next ? "xl" : "md"}
      keyboard={true}
    >
      <ModalHeader toggle={toggle}>Rincian {tittle}</ModalHeader>
      <ModalBody>
        {next ? (
          <>
            <FormRincianBarang
              hetInput={hetInput}
              barang={barang}
              valueRincian={valueRincian}
              handleChangeRincian={handleChangeRincian}
              buttonTambah={buttonTambah}
              rincian={rincian}
              deleteRincian={deleteRincian}
              alertR={alertR}
            />
          </>
        ) : (
          <Form>
            <Card>
              <CardHeader>
                <strong sm={6}>Asal Pesanan</strong>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Label sm={6}>Asal Pesanan</Label>
                  <Col sm={6}>
                    <Input
                      name="asal_pesanan"
                      value={order.asal_pesanan}
                      onChange={(e) => handleChangeFormOrder(e)}
                      placeholder="Asal Pesanan"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={6}>Tanggal Pemesanan</Label>
                  <Col sm={6}>
                    <Input
                      name="tgl_surat_pesanan"
                      type="date"
                      value={order.tgl_surat_pesanan}
                      onChange={(e) => handleChangeFormOrder(e)}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={6}>Nomor Pesanan</Label>
                  <Col sm={6}>
                    <Input
                      name="no_surat_pesanan"
                      value={order.no_surat_pesanan}
                      onChange={(e) => handleChangeFormOrder(e)}
                      placeholder="Nomor Surat"
                    />
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
            <br />
            <Card>
              <CardHeader>
                <strong>Tujuan Pesanan</strong>
              </CardHeader>
              <CardBody>
                <Label>Pilih Supplier</Label>
                <FormGroup row>
                  <Col sm={4}>
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="radio1"
                          onClick={() => {
                            setSupplier(true);
                            setForms(true);
                            handleSetFormBulog();
                          }}
                        />
                        Bulog
                      </Label>
                    </FormGroup>
                  </Col>
                  <Col sm={6}>
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="radio1"
                          onClick={() => {
                            setSupplier(false);
                            setForms(true);
                            handleChangeNullFormOrder();
                          }}
                        />{" "}
                        Lainnya
                      </Label>
                    </FormGroup>
                  </Col>
                </FormGroup>
                <br />
                {forms ? (
                  <>
                    <FormGroup row>
                      <Label sm={6}>Provinsi</Label>
                      <Col sm={6}>
                        <Form.Control
                          as="select"
                          name="tujuan_provinsi"
                          value={order.tujuan_provinsi}
                          onChange={async (e) => {
                            handleChangeFormOrder(e);
                            handleChangeKota(e);
                          }}
                        >
                          <option>Pilih Provinsi</option>
                          {kotaBulog.data.map((item, index) => (
                            <option key={index}>{item.provinsi}</option>
                          ))}
                        </Form.Control>
                      </Col>
                    </FormGroup>
                    {supplier ? null : (
                      <FormGroup row>
                        <Label sm={6}>Supplier</Label>
                        <Col sm={6}>
                          <Input
                            name="tujuan_supplier"
                            // value={order.tujuan_supplier}
                            onChange={(e) => handleChangeFormOrder(e)}
                            placeholder="Supplier"
                          />
                        </Col>
                      </FormGroup>
                    )}
                    {provinsi === "" ? null : (
                      <>
                        {supplier ? null : (
                          <FormGroup row>
                            <Label sm={6}>Kantor Supplier</Label>
                            <Col sm={6}>
                              <Form.Control
                                as="select"
                                name="daerah_tujuan_bulog"
                                value={order.daerah_tujuan_bulog}
                                onChange={(e) => handleChangeFormOrder(e)}
                              >
                                <option>Pilih Kota/Kabupaten</option>
                                {kota.data.map((item, index) => (
                                  <option key={index}>{item.wil_kab}</option>
                                ))}
                              </Form.Control>
                            </Col>
                          </FormGroup>
                        )}
                        <FormGroup row>
                          <Label sm={6}>Untuk Daerah</Label>
                          <Col sm={6}>
                            <Form.Control
                              as="select"
                              name="daerah_tujuan"
                              value={order.daerah_tujuan}
                              onChange={(e) => handleChangeFormOrder(e)}
                            >
                              <option>Pilih Kota/Kabupaten</option>
                              {kota.data.map((item, index) => (
                                <option key={index}>{item.wil_kab}</option>
                              ))}
                            </Form.Control>
                          </Col>
                        </FormGroup>
                      </>
                    )}
                    <FormGroup row>
                      <Label sm={6}>No Surat</Label>
                      <Col sm={6}>
                        <Input
                          name="no_surat_tujuan"
                          value={order.no_surat_tujuan}
                          onChange={(e) => handleChangeFormOrder(e)}
                          placeholder="Nomor Surat"
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Label sm={6}>Tanggal Surat</Label>
                      <Col sm={6}>
                        <Input
                          type="date"
                          name="tgl_surat_tujuan"
                          min={minTglTujuan}
                          value={order.tgl_surat_tujuan}
                          onChange={(e) => handleChangeFormOrder(e)}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Label sm={6}>Tanggal Pengambilan</Label>
                      <Col sm={6}>
                        <div style={{ textAlign: "center" }}>
                          <label>
                            {order.tgl_pengambilan === "Invalid date" ||
                            order.tgl_pengambilan === ""
                              ? null
                              : moment(order.tgl_pengambilan)
                                  .format("DD-MM-YYYY")
                                  .toString()}
                          </label>
                        </div>
                      </Col>
                    </FormGroup>
                  </>
                ) : null}
              </CardBody>
            </Card>
            <br />
            {alert ? (
              <Alert color="danger">Harap isi Form yang kosong</Alert>
            ) : null}
          </Form>
        )}
      </ModalBody>
      <ModalFooter>
        <Button
          onClick={() => {
            if (
              order.asal_pesanan === "" ||
              order.tgl_surat_pesanan === "" ||
              order.no_surat_pesanan === "" ||
              order.tujuan_provinsi === "" ||
              order.no_surat_tujuan === "" ||
              order.tgl_surat_tujuan === "" ||
              order.daerah_tujuan === "" ||
              order.tgl_pengambilan === "" ||
              order.tujuan_supplier === ""
            ) {
              setAlert(true);
            } else {
              setNext(!next);
              setbtext(
                btext === "Rincian Pesanan"
                  ? "Rincian Surat"
                  : "Rincian Pesanan"
              );
              setTittle(tittle === "Surat" ? "Barang" : "Surat");
            }
          }}
          color="primary"
          outline
        >
          {btext}
        </Button>
        {next ? (
          <>
            {buttonLoading ? (
              <Button color="primary" outline>
                <Image src={loading} style={{ width: 20, height: 20 }} />
              </Button>
            ) : (
              <Button color="primary" outline onClick={handleSubmit}>
                Selesai
              </Button>
            )}
          </>
        ) : null}
      </ModalFooter>
    </Modal>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    provinsiIndex: () => dispatch(provinsiIndex()),
    kotaBulogIndex: () => dispatch(kotaBulogIndex()),
    kotaIndex: (provinsi) => dispatch(kotaIndex(provinsi)),
    addPesanan: (order, rincian) => dispatch(addPesanan(order, rincian)),
    barangIndex: () => dispatch(barangIndex()),
    getTipeUnit: (unit) => dispatch(getTipeUnit(unit)),
    getHetBarang: (b, p) => dispatch(getHetBarang(b, p)),
  };
};
export default connect(null, mapDispatchToProps)(TambahPesananModal);
