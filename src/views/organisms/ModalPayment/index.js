import React, { useState, useEffect } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Table,
  Card,
  CardBody,
  CardHeader,
} from "reactstrap";

import { formatDate } from "../../../utils";
import { getRincianOrder, paymentUpdate } from "../../../redux/actions/list";
import { connect } from "react-redux";
import DetailPayment from "../../components/DetailPayment";
import DetailPaymentTagihan from "../../components/DetailPaymentTagihan";
import EditPayment from "../../components/EditPayment";
import ModalRiwayatPayment from "../../organisms/ModalRiwayatPayment";

const ModalPayment = (props) => {
  const { isOpen, toggle, data } = props;
  const [modalEdit, setModalEdit] = useState(false);
  const [paymentLogModal, setPaymentLogModal] = useState(false);
  const toggleEdit = () => setModalEdit(!modalEdit);
  const toggleLog = () => setPaymentLogModal(!paymentLogModal);

  const [mode, setMode] = useState(null);
  const [buttonLoading, setButtonLoading] = useState(false);

  const [stateRincian, setStateRincian] = useState({
    order: null,
    rincian: [],
    payment: null,
  });
  const [pay, setPay] = useState(null);
  const [paymentLogIn, setPaymentLogIn] = useState([]);
  const [paymentLogOut, setPaymentLogOut] = useState([]);
  const [logMode, setLogMode] = useState(null);

  useEffect(() => {
    const x = async () => {
      const res = await props.getRincianOrder(data?.id);
      if (res) {
        setStateRincian({
          order: res.action.payload?.order,
          payment: res.action.payload?.payment,
        });
        setPay({
          ...pay,
          bayar_bulog: res.action.payload?.payment.bayar_bulog,
          terima_bayar: res.action.payload?.payment.terima_bayar,
        });
        setPaymentLogIn(res.action.payload?.payment_log_in);
        setPaymentLogOut(res.action.payload?.payment_log_out);
        // console.log({ res });
      }
    };
    // if (isOpen === false) {
    //   window.location.reload();
    // }
    if (isOpen) x();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isOpen]);

  useEffect(() => {
    const x = async () => {
      const res = await props.getRincianOrder(data?.id);
      if (res) {
        setStateRincian({
          order: res.action.payload?.order,
          payment: res.action.payload?.payment,
        });
        setPay({
          ...pay,
          bayar_bulog: res.action.payload?.payment.bayar_bulog,
          terima_bayar: res.action.payload?.payment.terima_bayar,
        });
        setPaymentLogIn(res.action.payload?.payment_log_in);
        setPaymentLogOut(res.action.payload?.payment_log_out);
      }
    };
    x();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalEdit]);

  const handleChange = (e) => {
    setPay({
      ...pay,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (type) => {
    setButtonLoading(true);
    const x = await props.paymentUpdate(stateRincian.payment?.id, pay, type);
    if (x) {
      setModalEdit(false);
      setButtonLoading(false);
    }
  };

  return (
    <Modal
      isOpen={isOpen}
      toggle={toggle}
      backdrop="static"
      size="lg"
      keyboard={true}
    >
      <ModalHeader toggle={toggle}>Rincian Pembayaran</ModalHeader>
      <ModalBody>
        <DetailPayment stateRincian={stateRincian} />
        <br />
        <DetailPaymentTagihan
          stateRincian={stateRincian}
          setMode={setMode}
          setModalEdit={setModalEdit}
          setLogMode={setLogMode}
          toggleModalLog={toggleLog}
        />
        <>
          <br />
          <Card>
            <CardHeader>
              <strong>Rincian Pesanan</strong>
            </CardHeader>
            <CardBody>
              <Table bordered>
                {data && (
                  <tbody>
                    <tr>
                      <td>Tanggal Pengambilan</td>
                      <td>
                        {formatDate(data.tgl_pengambilan, "DD MMMM YYYY")}
                      </td>
                    </tr>
                    <tr>
                      <td>Status</td>
                      {stateRincian.order?.status === "Lunas" ? (
                        <td
                          style={{
                            backgroundColor: "green",
                            color: "white",
                            textAlign: "center",
                          }}
                        >
                          {stateRincian.order?.status}
                        </td>
                      ) : (
                        <td
                          style={{
                            backgroundColor: "red",
                            color: "white",
                            textAlign: "center",
                          }}
                        >
                          {stateRincian.order?.status}
                        </td>
                      )}
                    </tr>
                  </tbody>
                )}
              </Table>
            </CardBody>
          </Card>
        </>
      </ModalBody>

      <EditPayment
        data={data}
        pay={pay}
        handleChange={handleChange}
        stateRincian={stateRincian}
        handleSubmit={handleSubmit}
        toggleEdit={toggleEdit}
        modalEdit={modalEdit}
        buttonLoading={buttonLoading}
        mode={mode}
      />

      <ModalRiwayatPayment
        paymentLogIn={paymentLogIn}
        paymentLogOut={paymentLogOut}
        mode={logMode}
        toggle={toggleLog}
        isOpen={paymentLogModal}
      />
    </Modal>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getRincianOrder: (id) => dispatch(getRincianOrder(id)),
    paymentUpdate: (id, pay, type) => dispatch(paymentUpdate(id, pay, type)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ModalPayment);
