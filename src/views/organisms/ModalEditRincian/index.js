import React, { useEffect, useState } from "react";

import {
  Col,
  FormGroup,
  Label,
  Input,
  Row,
  Modal,
  ModalBody,
  Button,
  ModalFooter,
  ModalHeader,
  Alert,
} from "reactstrap";

import { Table, Form, Image } from "react-bootstrap";
import {
  barangIndex,
  getHetBarang,
  getTipeUnit,
} from "../../../redux/actions/list";
import { connect } from "react-redux";
import loading from "../../../assets/loading.svg";

const FormRincianBarang = ({
  isOpen,
  toggle,
  data,
  setData,
  barangIndex,
  handleEditRincian,
  buttonLoading,
  provinsi,
  getHetBarang,
  getTipeUnit,
  alert,
  setAlert,
}) => {
  const [barang, setBarang] = useState([]);

  useEffect(() => {
    const dataBarang = async () => {
      const res = await barangIndex();
      setBarang(res.action.payload);
    };
    if (isOpen) {
      dataBarang();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isOpen]);

  useEffect(() => {
    const getunit = async () => {
      const nama = data?.nama_barang;
      const resHet = await getHetBarang(nama, provinsi);
      const res = await getTipeUnit(nama);
      if (nama === null) {
        return null;
      } else {
        setData({
          ...data,
          tipe_unit: res.action.payload?.tipe_unit,
          het: resHet.action.payload?.het || "",
        });
      }
    };
    getunit();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data?.nama_barang]);

  const handleChangeRincian = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
    setAlert({ ...alert, alert: false });
  };

  return (
    data && (
      <Modal size="xl" isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>Ubah Rincian Barang</ModalHeader>
        <ModalBody>
          {alert.alert ? (
            <Alert color="danger">Harap Isi Form Yang Kosong</Alert>
          ) : null}
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Nama Barang</th>
                <th>Unit</th>
                <th>Jumlah</th>
                <th>Harga Per Satuan</th>
                <th>HET</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td width="20%">
                  {barang && (
                    <FormGroup>
                      <Form.Control
                        as="select"
                        name="nama_barang"
                        value={data.nama_barang}
                        onChange={(e) => handleChangeRincian(e)}
                      >
                        <option value="">Pilih Barang</option>
                        {barang.map((item, index) => (
                          <option key={index}>{item.nama}</option>
                        ))}
                      </Form.Control>
                    </FormGroup>
                  )}
                </td>
                <td width="15%">
                  <Row>
                    <Col sm={8}>
                      <Input
                        name="unit"
                        type="number"
                        value={data.unit}
                        onChange={(e) => {
                          handleChangeRincian(e);
                        }}
                      />
                    </Col>
                    <Col sm={2}>
                      <label>{data.tipe_unit}</label>
                    </Col>
                  </Row>
                </td>
                <td width="10%">
                  <FormGroup>
                    <Input
                      name="jumlah"
                      type="number"
                      value={data.jumlah}
                      onChange={(e) => handleChangeRincian(e)}
                    />
                  </FormGroup>
                </td>
                <td>
                  <FormGroup>
                    <Row>
                      <Col sm={1}>
                        <Label>Rp</Label>
                      </Col>
                      <Col sm={10}>
                        <Input
                          name="harga_perunit"
                          type="number"
                          value={data.harga_perunit}
                          onChange={(e) => handleChangeRincian(e)}
                        />
                      </Col>
                    </Row>
                  </FormGroup>
                </td>
                <td>
                  <FormGroup>
                    <Row>
                      <Col sm={1}>
                        <Label>Rp</Label>
                      </Col>
                      <Col sm={10}>
                        <Input
                          name="het"
                          type="number"
                          value={data.het}
                          onChange={(e) => handleChangeRincian(e)}
                        />
                      </Col>
                    </Row>
                  </FormGroup>
                </td>
              </tr>
            </tbody>
          </Table>
        </ModalBody>
        <ModalFooter>
          {buttonLoading ? (
            <Button color="primary" outline>
              <Image src={loading} style={{ width: 20, height: 20 }} />
            </Button>
          ) : (
            <Button color="success" onClick={handleEditRincian}>
              Selesai
            </Button>
          )}
        </ModalFooter>
      </Modal>
    )
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    barangIndex: () => dispatch(barangIndex()),
    getHetBarang: (b, p) => dispatch(getHetBarang(b, p)),
    getTipeUnit: (unit) => dispatch(getTipeUnit(unit)),
  };
};

export default connect(null, mapDispatchToProps)(FormRincianBarang);
