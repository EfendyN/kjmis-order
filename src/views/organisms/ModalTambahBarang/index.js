import React, { useState, useEffect } from "react";
import {
  Modal,
  ModalHeader,
  Button,
  Card,
  CardBody,
  ModalFooter,
  Alert,
  Input,
  FormGroup,
  Label,
  Col,
  FormFeedback,
} from "reactstrap";

import { Image } from "react-bootstrap";

import { connect } from "react-redux";
import { postBarang } from "../../../redux/actions/barang";
import loading from "../../../assets/loading.svg";

const ModalUpdatePassword = ({ isOpen, toggle, postBarang }) => {
  const [values, setValues] = useState({
    nama: "",
    tipe_unit: "",
  });

  const [alert, setAlert] = useState({
    alert: false,
    success: false,
    nama: false,
    satuan: false,
  });
  const [buttonLoading, setButtonLoading] = useState(false);

  useEffect(() => {
    setAlert({ alert: false, success: false, nama: false, satuan: false });
    setValues({ nama: "", tipe_unit: "" });
  }, [isOpen]);

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
    setAlert({ alert: false, nama: false, satuan: false });
  };

  const handleSubmit = async () => {
    if (values.nama === "") {
      setAlert({ ...alert, nama: true });
    } else if (values.tipe_unit === "") {
      setAlert({ ...alert, satuan: true });
    } else {
      setButtonLoading(true);
      const x = await postBarang(values);
      if (x.action.payload.message === "ada") {
        setAlert({ ...alert, alert: true, success: false });
        setValues({ nama: "", tipe_unit: "" });
        setButtonLoading(false);
      } else {
        setButtonLoading(false);
        setValues({ nama: "", tipe_unit: "" });
        setAlert({ ...alert, success: true, alert: false });
      }
    }
  };
  return (
    <Modal
      isOpen={isOpen}
      toggle={toggle}
      backdrop="static"
      size="md"
      keyboard={true}
    >
      <ModalHeader toggle={toggle}>Tambah Barang</ModalHeader>
      <Card>
        <CardBody>
          {alert.alert ? <Alert color="danger">Barang Sudah Ada</Alert> : null}
          {alert.success ? (
            <Alert color="success">Barang Berhasil Ditambahkan</Alert>
          ) : null}

          <FormGroup row>
            <Label sm={4}>Nama Barang</Label>
            <Col sm={8}>
              <Input
                name="nama"
                invalid={alert.nama}
                value={values.nama}
                placeholder="Masukkan Nama Barang"
                onChange={(e) => handleChange(e)}
              />
            </Col>
            <FormFeedback>Nama Barang Tidak Boleh Kosong</FormFeedback>
          </FormGroup>
          <FormGroup row>
            <Label sm={4}>Satuan</Label>
            <Col sm={8}>
              <Input
                name="tipe_unit"
                invalid={alert.satuan}
                value={values.tipe_unit}
                placeholder="Masukkan Satuan"
                onChange={(e) => handleChange(e)}
              />
            </Col>
            <FormFeedback>Satuan Tidak Boleh Kosong</FormFeedback>
          </FormGroup>
        </CardBody>
      </Card>
      <ModalFooter>
        {buttonLoading ? (
          <Button color="primary" outline>
            <Image src={loading} style={{ width: 20, height: 20 }} />
          </Button>
        ) : (
          <Button color="success" onClick={handleSubmit}>
            Tambah Barang
          </Button>
        )}
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    postBarang: (values) => dispatch(postBarang(values)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalUpdatePassword);
