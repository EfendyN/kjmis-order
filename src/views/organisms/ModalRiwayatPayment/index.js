import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import { Table } from "react-bootstrap";
import { formatDate, formatNumber } from "../../../utils";

const ModalRiwayatPayment = ({
  paymentLogOut,
  paymentLogIn,
  mode,
  isOpen,
  toggle,
}) => {
  return (
    <Modal
      isOpen={isOpen}
      toggle={toggle}
      backdrop="static"
      size="lg"
      keyboard={true}
    >
      <ModalHeader toggle={toggle}>
        {mode === "IN"
          ? "Riwayat Terima Pembayaran"
          : mode === "OUT"
          ? "Riwayat Pembayaran"
          : null}
      </ModalHeader>
      <ModalBody>
        {mode === "IN" ? (
          <Table striped bordered hover responsive>
            <thead>
              <tr>
                <th>#</th>
                <th>Total Bayar</th>
                <th>Tanggal</th>
                <th>#</th>
              </tr>
            </thead>
            <tbody>
              {paymentLogIn.map((item, index) => (
                <tr key={index}>
                  <th>{index + 1}</th>
                  <td>Rp{formatNumber(item.bayar)},-</td>
                  <td>{formatDate(item.tgl_bayar, "DD MMMM YYYY")}</td>
                  <td>
                    <div style={{ color: item.selisih < 0 ? "red" : "green" }}>
                      {item.selisih < 0 ? "" : "+"}
                      {formatNumber(item.selisih)}
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : mode === "OUT" ? (
          <Table striped bordered hover responsive>
            <thead>
              <tr>
                <th>#</th>
                <th>Total Bayar</th>
                <th>Tanggal</th>
                <th>#</th>
              </tr>
            </thead>
            <tbody>
              {paymentLogOut.map((item, index) => (
                <tr key={index}>
                  <th>{index + 1}</th>
                  <td>Rp{formatNumber(item.bayar)},-</td>
                  <td>{formatDate(item.tgl_bayar, "DD MMMM YYYY")}</td>
                  <td>
                    <div style={{ color: item.selisih < 0 ? "red" : "green" }}>
                      {item.selisih < 0 ? "" : "+"}
                      {formatNumber(item.selisih)}
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : null}
      </ModalBody>
    </Modal>
  );
};

export default ModalRiwayatPayment;
