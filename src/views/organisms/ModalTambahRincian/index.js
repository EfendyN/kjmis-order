import React, { useEffect, useState } from "react";

import {
  Col,
  FormGroup,
  Label,
  Input,
  Row,
  Modal,
  ModalBody,
  Button,
  ModalFooter,
  ModalHeader,
  Alert,
} from "reactstrap";

import { Table, Form, Image } from "react-bootstrap";
import { createRincian } from "../../../redux/actions/order";
import {
  barangIndex,
  getHetBarang,
  getTipeUnit,
} from "../../../redux/actions/list";
import { connect } from "react-redux";
import load from "../../../assets/loading.svg";

const ModalTambahRincian = ({
  isOpen,
  toggle,
  createRincian,
  barangIndex,
  provinsi,
  getHetBarang,
  getTipeUnit,
  toggleModalTambahRincian,
  orderid,
}) => {
  const [barang, setBarang] = useState([]);
  const [loading, setLoading] = useState(false);
  const [alert, setAlert] = useState(false);
  const [data, setData] = useState({
    nama_barang: "",
    unit: "",
    tipe_unit: "",
    jumlah: "",
    harga_perunit: "",
    het: "",
  });

  useEffect(() => {
    const dataBarang = async () => {
      const res = await barangIndex();
      setBarang(res.action.payload);
    };
    if (isOpen) {
      dataBarang();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isOpen]);

  useEffect(() => {
    const getunit = async () => {
      const nama = data.nama_barang;
      const resHet = await getHetBarang(nama, provinsi);
      const res = await getTipeUnit(nama);
      if (nama === null) {
        return null;
      } else {
        setData({
          ...data,
          tipe_unit: res.action.payload?.tipe_unit,
          het: resHet.action.payload?.het || "",
        });
      }
    };
    getunit();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data.nama_barang]);

  const handleChangeRincian = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
    setAlert(false);
  };

  const handleTambahRincian = async () => {
    if (
      data?.nama_barang === "" ||
      data?.unit === "" ||
      data?.jumlah === "" ||
      data?.harga_perunit === "" ||
      data?.het === ""
    ) {
      setAlert(true);
    } else {
      setLoading(true);
      const x = await createRincian(orderid, data);
      if (x) {
        setLoading(false);
        toggleModalTambahRincian();
        setData({
          nama_barang: "",
          unit: "",
          tipe_unit: "",
          jumlah: "",
          harga_perunit: "",
          het: "",
        });
      }
    }
  };

  return (
    <Modal size="xl" isOpen={isOpen} toggle={toggle}>
      <ModalHeader toggle={toggle}>Tambah Rincian Barang</ModalHeader>
      <ModalBody>
        {alert ? (
          <Alert color="danger">Harap isi Form Yang Kosong</Alert>
        ) : null}
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Nama Barang</th>
              <th>Unit</th>
              <th>Jumlah</th>
              <th>Harga Per Satuan</th>
              <th>HET</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td width="20%">
                {barang && (
                  <FormGroup>
                    <Form.Control
                      as="select"
                      name="nama_barang"
                      value={data.nama_barang}
                      onChange={(e) => handleChangeRincian(e)}
                    >
                      <option value="">Pilih Barang</option>
                      {barang.map((item, index) => (
                        <option key={index}>{item.nama}</option>
                      ))}
                    </Form.Control>
                  </FormGroup>
                )}
              </td>
              <td width="15%">
                <Row>
                  <Col sm={8}>
                    <Input
                      name="unit"
                      type="number"
                      value={data.unit}
                      onChange={(e) => {
                        handleChangeRincian(e);
                      }}
                    />
                  </Col>
                  <Col sm={2}>
                    <label>{data.tipe_unit}</label>
                  </Col>
                </Row>
              </td>
              <td width="10%">
                <FormGroup>
                  <Input
                    name="jumlah"
                    type="number"
                    value={data.jumlah}
                    onChange={(e) => handleChangeRincian(e)}
                  />
                </FormGroup>
              </td>
              <td>
                <FormGroup>
                  <Row>
                    <Col sm={1}>
                      <Label>Rp</Label>
                    </Col>
                    <Col sm={10}>
                      <Input
                        name="harga_perunit"
                        type="number"
                        value={data.harga_perunit}
                        onChange={(e) => handleChangeRincian(e)}
                      />
                    </Col>
                  </Row>
                </FormGroup>
              </td>
              <td>
                <FormGroup>
                  <Row>
                    <Col sm={1}>
                      <Label>Rp</Label>
                    </Col>
                    <Col sm={10}>
                      <Input
                        name="het"
                        type="number"
                        value={data.het}
                        onChange={(e) => handleChangeRincian(e)}
                      />
                    </Col>
                  </Row>
                </FormGroup>
              </td>
            </tr>
          </tbody>
        </Table>
      </ModalBody>
      <ModalFooter>
        {loading ? (
          <Button color="primary" outline>
            <Image src={load} style={{ width: 20, height: 20 }} />
          </Button>
        ) : (
          <Button color="success" onClick={handleTambahRincian}>
            Tambah
          </Button>
        )}
      </ModalFooter>
    </Modal>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    createRincian: (id, dataRincian) =>
      dispatch(createRincian(id, dataRincian)),
    barangIndex: () => dispatch(barangIndex()),
    getHetBarang: (b, p) => dispatch(getHetBarang(b, p)),
    getTipeUnit: (unit) => dispatch(getTipeUnit(unit)),
  };
};

export default connect(null, mapDispatchToProps)(ModalTambahRincian);
