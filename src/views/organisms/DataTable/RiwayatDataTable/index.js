import React from "react";
import ReactTable from "react-table-6";
// import { Row, Col, Button } from "reactstrap";
import { Table } from "react-bootstrap";
import { connect } from "react-redux";
// Helpers

// Utils
import { formatDate } from "../../../../utils";
import DownloadTable from "react-html-table-to-excel";

// Actions
import { getDetail } from "../../../../redux/actions/list";

const HistoryDataTable = ({
  //   getDetail,
  //   handleClikDetail,
  //   handleClikPayment,
  dataTable,
}) => {
  return (
    <>
      <div>
        <DownloadTable
          table="table-data-riwayat"
          filename={`Data Pesanan ${new Date()}`}
          sheet="sheet 1"
        />
      </div>
      <br />
      <Table id="table-data-riwayat" striped responsive>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Barang</th>
            <th>Unit</th>
            <th>Satuan</th>
            <th>Jumlah</th>
            <th>Total Harga</th>
            <th>Asal Pesanan</th>
            <th>Tanggal Pesanan</th>
            <th>Supplier</th>
            <th>Provinsi</th>
            <th>Tanggal Tujuan</th>
            <th>Daerah</th>
            <th>Tanggal Pengambilan</th>
          </tr>
        </thead>
        <tbody>
          {dataTable.data.map((item, index) => (
            <tr key={index}>
              <th>{index + 1}</th>
              <th>{item.nama_barang}</th>
              <th>{item.unit}</th>
              <th>{item.tipe_unit}</th>
              <th>{item.jumlah}</th>
              <th>{item.total_harga}</th>
              <th>{item.order.asal_pesanan}</th>
              <th>{formatDate(item.order.tgl_surat_pesanan, "DD/MM/YYYY")}</th>
              <th>{item.order.tujuan_supplier}</th>
              <th>{item.order.tujuan_provinsi}</th>
              <th>{formatDate(item.order.tgl_surat_tujuan, "DD/MM/YYYY")}</th>
              <th>{item.order.daerah_tujuan}</th>
              <th>{formatDate(item.order.tgl_pengambilan, "DD/MM/YYYY")}</th>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return { getDetail: (id) => dispatch(getDetail(id)) };
};
export default connect(mapStateToProps, mapDispatchToProps)(HistoryDataTable);
