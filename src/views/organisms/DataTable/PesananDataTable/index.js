import React, { useEffect, useState } from "react";
import ReactTable from "react-table-6";
import { Row, Col, Input, Button } from "reactstrap";
import { Form, Image } from "react-bootstrap";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
// Helpers

// Utils
import { formatDate } from "../../../../utils";
import refresh from "../../../components/image/refresh.png";
// Actions
import { listIndex, getDetail } from "../../../../redux/actions/list";

const PesananDataTable = ({
  listIndex,
  getDetail,
  handleClikDetail,
  handleClikPayment,
  modalDetailIsOpen,
  modalPayment,
}) => {
  const [stateTable, setStateTable] = useState({
    data: [],
    page: 0,
    pageSize: 20,
    loading: false,
  });

  const [search, setSearch] = useState(false);
  const [values, setValues] = useState({
    asal_pesanan: "",
    tgl_surat_pesanan: "",
    no_surat_pesanan: "",
    tujuan_provinsi: "",
    tujuan_supplier: "",
    no_surat_tujuan: "",
    tgl_surat_tujuan: "",
    daerah_tujuan: "",
    tgl_pengambilan: "",
    status: "",
  });

  useEffect(() => {
    const data = async () => {
      const res = await listIndex(values);
      setStateTable({ data: res.action.payload });
      // if (res) {
      //   console.log({ res });
      // }
    };
    data();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const data = async () => {
      const res = await listIndex(values);
      setStateTable({ data: res.action.payload });
    };
    data();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalDetailIsOpen]);

  useEffect(() => {
    const data = async () => {
      const res = await listIndex(values);
      setStateTable({ data: res.action.payload });
    };
    data();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalPayment]);

  useEffect(() => {
    const data = async () => {
      const res = await listIndex(values);
      setStateTable({ data: res.action.payload });
    };
    if (
      values.status ||
      values.asal_pesanan ||
      values.tgl_surat_pesanan ||
      values.tujuan_provinsi ||
      values.no_surat_tujuan ||
      values.tgl_surat_tujuan ||
      values.daerah_tujuan ||
      values.tgl_pengambilan ||
      search
    ) {
      data();
      setSearch(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search]);

  const handleChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };

  const handleRefresh = () => {
    setValues({
      asal_pesanan: "",
      tgl_surat_pesanan: "",
      no_surat_pesanan: "",
      tujuan_provinsi: "",
      no_surat_tujuan: "",
      tgl_surat_tujuan: "",
      daerah_tujuan: "",
      tgl_pengambilan: "",
      status: "",
    });
    setSearch(true);
  };
  return (
    <ReactTable
      data={stateTable.data}
      // page={stateTable.page}
      // pages={stateTable.pages}
      // loading={stateTable.loading}
      // defaultPageSize={stateTable.pageSize}
      filterable={true}
      manual={true}
      // onFetchData={handleFetchData}
      className="-striped -highlight"
      columns={[
        {
          Header: "Aksi",
          filterable: true,
          sortable: false,
          width: 150,
          Filter: () => (
            // <Button outline color="success" onClick={() => handleRefresh()}>
            <Image
              onClick={() => handleRefresh()}
              src={refresh}
              style={{ cursor: "pointer" }}
            />
            // </Button>
          ),
          Cell: (row) => {
            return (
              <Row>
                <Col className="text-center">
                  <Button
                    color="success"
                    size="sm"
                    onClick={async () => {
                      const x = await getDetail(row.original.order.id);
                      if (x) {
                        handleClikDetail(row.original);
                      }
                    }}
                  >
                    Rincian
                  </Button>{" "}
                  <Link to={`/print/${row.original.order.id}`}>
                    <Button color="success" size="sm">
                      Cetak
                    </Button>
                  </Link>
                </Col>
              </Row>
            );
          },
        },
        {
          Header: "ID",
          accessor: "order.id",
          width: 50,
          filterable: false,
          Filter: () => <Input name="id" />,
        },
        {
          Header: "Asal Pesanan",
          accessor: "order.asal_pesanan",
          Filter: () => (
            <Input
              name="asal_pesanan"
              defaultValue={values.asal_pesanan || ""}
              onKeyPress={(e) => {
                if (e.keyCode === 13 || e.which === 13) {
                  handleChange(e);
                  setSearch(true);
                }
              }}
            />
          ),
        },
        {
          Header: "Tanggal Surat Pesanan",
          accessor: "order.tgl_surat_pesanan",
          Filter: () => (
            <Input
              name="tgl_surat_pesanan"
              defaultValue={values.tgl_surat_pesanan || ""}
              type="date"
              onChange={(e) => {
                handleChange(e);
                setSearch(true);
              }}
            />
          ),
          Cell: (row) =>
            formatDate(row.original.order?.tgl_surat_pesanan, "DD/MM/YYYY"),
        },
        {
          Header: "Provinsi",
          accessor: "order.tujuan_provinsi",
          Filter: () => (
            <Input
              name="tujuan_provinsi"
              defaultValue={values.tujuan_provinsi || ""}
              onKeyPress={(e) => {
                if (e.keyCode === 13 || e.which === 13) {
                  handleChange(e);
                  setSearch(true);
                }
              }}
            />
          ),
        },
        {
          Header: "Supplier",
          accessor: "order.tujuan_supplier",
          filterable: "false",
          Filter: () => (
            <Input
              name="tujuan_supplier"
              defaultValue={values.tujuan_supplier || ""}
              onKeyPress={(e) => {
                if (e.keyCode === 13 || e.which === 13) {
                  handleChange(e);
                  setSearch(true);
                }
              }}
            />
          ),
        },
        {
          Header: "No Surat Tujuan",
          accessor: "order.no_surat_tujuan",
          Filter: () => (
            <Input
              name="no_surat_tujuan"
              defaultValue={values.no_surat_tujuan || ""}
              onKeyPress={(e) => {
                if (e.keyCode === 13 || e.which === 13) {
                  handleChange(e);
                  setSearch(true);
                }
              }}
            />
          ),
        },
        {
          Header: "Tgl Surat Tujuan",
          accessor: "order.tgl_surat_tujuan",
          Filter: () => (
            <Input
              name="tgl_surat_tujuan"
              defaultValue={values.tgl_surat_tujuan || ""}
              type="date"
              onChange={(e) => {
                handleChange(e);
                setSearch(true);
              }}
            />
          ),
          Cell: (row) =>
            formatDate(row.original.order?.tgl_surat_tujuan, "DD/MM/YYYY"),
        },
        {
          Header: "Daerah Tujuan",
          accessor: "order.daerah_tujuan",
          Filter: () => (
            <Input
              name="daerah_tujuan"
              defaultValue={values.daerah_tujuan || ""}
              onKeyPress={(e) => {
                if (e.keyCode === 13 || e.which === 13) {
                  handleChange(e);
                  setSearch(true);
                }
              }}
            />
          ),
        },

        {
          Header: "Tgl Pengambilan",
          accessor: "order.tgl_pengambilan",
          Filter: () => (
            <Input
              name="tgl_pengambilan"
              defaultValue={values.tgl_pengambilan || ""}
              type="date"
              onChange={(e) => {
                handleChange(e);
                setSearch(true);
              }}
            />
          ),
          Cell: (row) =>
            formatDate(row.original.order?.tgl_pengambilan, "DD/MM/YYYY"),
        },
        {
          Header: "Status",
          accessor: "order.status",
          Filter: () => (
            <Form.Control
              as="select"
              name="status"
              value={values.status}
              onChange={(e) => {
                handleChange(e);
                setSearch(true);
              }}
            >
              <option value="">Semua Status</option>
              <option value="Lunas">Lunas</option>
              <option value="Tunggakan">Tunggakan</option>
            </Form.Control>
          ),
          Cell: (row) => {
            if (row.original.order?.status === "Tunggakan") {
              return (
                <div
                  style={{
                    backgroundColor:
                      row.original.balance_terima < 0 &&
                      // row.original.balance_bulog >= 0 &&
                      row.original.terima_bayar > 0
                        ? "orange"
                        : "red",
                    textAlign: "center",
                    color: "white",
                    cursor: "pointer",
                  }}
                  onClick={() => handleClikPayment(row.original.order)}
                >
                  <label
                    style={{
                      cursor: "pointer",
                    }}
                  >
                    {row.original.order.status}
                  </label>
                </div>
              );
            } else {
              return (
                <div
                  style={{
                    backgroundColor: "green",
                    textAlign: "center",
                    color: "white",
                    cursor: "pointer",
                  }}
                  onClick={() => handleClikPayment(row.original.order)}
                >
                  <label
                    style={{
                      cursor: "pointer",
                    }}
                  >
                    {row.original.order?.status}
                  </label>
                </div>
              );
            }
          },
        },
      ]}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    list: state.list,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    listIndex: (values) => dispatch(listIndex(values)),
    getDetail: (id) => dispatch(getDetail(id)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(PesananDataTable);
