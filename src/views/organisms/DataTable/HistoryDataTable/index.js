import React from "react";
import ReactTable from "react-table-6";
import { Row, Col, Button } from "reactstrap";
import { connect } from "react-redux";
// Helpers

// Utils
import { formatDate } from "../../../../utils";

// Actions
import { getDetail } from "../../../../redux/actions/list";

const HistoryDataTable = ({
  getDetail,
  handleClikDetail,
  handleClikPayment,
  dataTable,
}) => {
  return (
    <ReactTable
      data={dataTable.data}
      manual={true}
      className="-striped -highlight"
      showPagination={false}
      showPaginationTop={false}
      showPaginationBottom={false}
      columns={[
        {
          Header: "Aksi",
          filterable: false,
          sortable: false,
          width: 100,
          Cell: (row) => {
            return (
              <Row>
                <Col className="text-center">
                  <Button
                    color="success"
                    size="sm"
                    onClick={async () => {
                      const x = await getDetail(row.original.id);
                      if (x) {
                        handleClikDetail(row.original);
                      }
                    }}
                  >
                    Rincian
                  </Button>
                </Col>
              </Row>
            );
          },
        },
        {
          Header: "ID",
          accessor: "order.id",
        },
        {
          Header: "Asal Pesanan",
          accessor: "order.asal_pesanan",
        },
        {
          Header: "Tgl Surat Pesanan",
          accessor: "order.tgl_surat_pesanan",

          Cell: (row) =>
            formatDate(row.original.order.tgl_surat_pesanan, "DD/MM/YYYY"),
        },
        {
          Header: "Provinsi",
          accessor: "order.tujuan_provinsi",
        },
        {
          Header: "Supplier",
          accessor: "order.tujuan_supplier",
        },
        {
          Header: "Tgl Surat Tujuan",
          accessor: "order.tgl_surat_tujuan",
          Cell: (row) =>
            formatDate(row.original.order.tgl_surat_tujuan, "DD/MM/YYYY"),
        },
        {
          Header: "Status",
          accessor: "order.status",

          Cell: (row) => {
            if (row.original.order.status === "Tunggakan") {
              return (
                <div
                  style={{
                    backgroundColor:
                      row.original.balance_terima < 0 &&
                      // row.original.balance_bulog >= 0 &&
                      row.original.terima_bayar > 0
                        ? "orange"
                        : "red",
                    textAlign: "center",
                    color: "white",
                    cursor: "pointer",
                  }}
                  onClick={() => handleClikPayment(row.original.order)}
                >
                  <label
                    style={{
                      cursor: "pointer",
                    }}
                  >
                    {row.original.order?.status}
                  </label>
                </div>
              );
            } else {
              return (
                <div
                  style={{
                    backgroundColor: "green",
                    textAlign: "center",
                    color: "white",
                    cursor: "pointer",
                  }}
                  onClick={() => handleClikPayment(row.original.order)}
                >
                  <label
                    style={{
                      cursor: "pointer",
                    }}
                  >
                    {row.original.order?.status}
                  </label>
                </div>
              );
            }
          },
        },
      ]}
    />
  );
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return { getDetail: (id) => dispatch(getDetail(id)) };
};
export default connect(mapStateToProps, mapDispatchToProps)(HistoryDataTable);
