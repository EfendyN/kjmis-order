import React, { useEffect, useState } from "react";
import ReactTable from "react-table-6";
import {
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Table,
} from "reactstrap";
import { connect } from "react-redux";
// Helpers

// Utils
// Actions
import { getBarangIndex, deleteBarang } from "../../../../redux/actions/barang";

const HistoryDataTable = ({ getBarangIndex, deleteBarang, modalTambah }) => {
  const [dataTable, setDataTable] = useState([]);
  const [modalHapus, setModalHapus] = useState(false);
  const [data, setData] = useState({});
  useEffect(() => {
    const data = async () => {
      const x = await getBarangIndex();
      if (x) {
        // console.log(x);
        setDataTable(x.action.payload);
      }
    };
    data();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const data = async () => {
      const x = await getBarangIndex();
      if (x) {
        console.log(x);
        setDataTable(x.action.payload);
      }
    };
    data();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalTambah]);

  const toggleModalHapus = (x) => {
    setModalHapus(!modalHapus);
    setData(x);
  };

  const handleDelete = async () => {
    const id = data?.id;
    const x = await deleteBarang(id);
    if (x) {
      window.location.reload();
    }
  };

  return (
    <>
      <ReactTable
        data={dataTable}
        manual={true}
        className="-striped -highlight"
        showPagination={false}
        showPaginationTop={false}
        showPaginationBottom={false}
        columns={[
          {
            Header: "Aksi",
            filterable: false,
            sortable: false,
            width: 100,
            Cell: (row) => {
              return (
                <Row>
                  <Col className="text-center">
                    <Button
                      color="danger"
                      size="sm"
                      onClick={(x) => toggleModalHapus(row.original)}
                    >
                      Hapus
                    </Button>
                  </Col>
                </Row>
              );
            },
          },
          {
            width: 50,
            Header: "No",
            Cell: (row) => {
              return <label>{row.index + 1}</label>;
            },
          },
          {
            Header: "Nama Barang",
            accessor: "nama",
          },
          {
            width: 100,
            Header: "Tipe Satuan",
            accessor: "tipe_unit",
          },
        ]}
      />

      {data && (
        <Modal size="sm" isOpen={modalHapus} toggle={toggleModalHapus}>
          <ModalHeader toggle={toggleModalHapus}>Hapus Barang?</ModalHeader>
          <ModalBody>
            <Table responsive bordered>
              <thead>
                <tr>
                  <th>Nama</th>
                  <th>Satuan</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{data.nama}</td>
                  <td>{data.tipe_unit}</td>
                </tr>
              </tbody>
            </Table>
          </ModalBody>
          <ModalFooter>
            <Button>Batal</Button>
            <Button onClick={() => handleDelete()}>Hapus</Button>
          </ModalFooter>
        </Modal>
      )}
    </>
  );
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    getBarangIndex: () => dispatch(getBarangIndex()),
    deleteBarang: (id) => dispatch(deleteBarang(id)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(HistoryDataTable);
