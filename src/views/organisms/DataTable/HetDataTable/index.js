import React, { useEffect, useState } from "react";
import ReactTable from "react-table-6";

import { Form } from "react-bootstrap";
import { connect } from "react-redux";
// Helpers
import ModalTambahHet from "../../ModalTambahHet";
// Utils
import { formatDate, formatNumber } from "../../../../utils";
// Actions
import { getHetIndex, getBarangIndex } from "../../../../redux/actions/barang";
import { kotaBulogIndex } from "../../../../redux/actions/list";

const HistoryDataTable = ({
  getHetIndex,
  kotaBulogIndex,
  getBarangIndex,
  setDataBarangs,
  setDataWilayah,
  modalTambahHET,
  togleModalTambahHET,
}) => {
  const [dataTable, setDataTable] = useState([]);
  const [search, setSearch] = useState(false);
  const [dataBarang, setDataBarang] = useState([]);
  const [wilayah, setWilayah] = useState([]);
  const [values, setValues] = useState({
    nama_barang: "",
    wilayah: "",
    status: "Aktif",
  });

  useEffect(() => {
    const data = async () => {
      const x = await getHetIndex(values);
      if (x) {
        setDataTable(x.action.payload);
      }
    };
    const GetDataBarang = async () => {
      const x = await getBarangIndex();
      if (x) {
        setDataBarang(x.action.payload);
        setDataBarangs(x.action.payload);
      }
    };
    const dataWilayah = async () => {
      const res = await kotaBulogIndex();
      setWilayah(res.action.payload);
      setDataWilayah(res.action.payload);
    };
    data();
    dataWilayah();
    GetDataBarang();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const data = async () => {
      const x = await getHetIndex(values);
      if (x) {
        // console.log(x);
        setDataTable(x.action.payload);
      }
    };
    data();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalTambahHET]);

  useEffect(() => {
    const data = async () => {
      const x = await getHetIndex(values);
      if (x) {
        // console.log(x);
        setDataTable(x.action.payload);
        setSearch(false);
      }
    };
    if (search) data();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search]);

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  return (
    <>
      <ReactTable
        data={dataTable}
        manual={true}
        className="-striped -highlight"
        showPagination={false}
        showPaginationTop={false}
        showPaginationBottom={false}
        columns={[
          {
            width: 50,
            Header: "No",
            Cell: (row) => {
              return <label>{row.index + 1}</label>;
            },
          },
          {
            Header: "Nama Barang",
            accessor: "nama_barang",
            filterable: true,
            Filter: () => (
              <>
                <Form.Group>
                  <Form.Control
                    as="select"
                    name="nama_barang"
                    value={values.nama_barang}
                    onChange={(e) => {
                      handleChange(e);
                      setSearch(true);
                    }}
                  >
                    <option value="">Semua Barang</option>
                    {dataBarang.map((item, index) => (
                      <option key={index}>{item.nama}</option>
                    ))}
                  </Form.Control>
                </Form.Group>
              </>
            ),
          },
          {
            Header: "Wilayah",
            accessor: "wilayah",
            filterable: true,
            Filter: () => (
              <Form.Group>
                <Form.Control
                  as="select"
                  name="wilayah"
                  value={values.wilayah}
                  onChange={(e) => {
                    handleChange(e);
                    setSearch(true);
                  }}
                >
                  <option value="">Semua Wilayah</option>
                  {wilayah.map((item, index) => (
                    <option key={index}>{item.provinsi}</option>
                  ))}
                </Form.Control>
              </Form.Group>
            ),
          },
          {
            width: 220,
            Header: "Ditambahkan Pada Tanggal",
            accessor: "tgl_berlaku",
            Cell: (row) => formatDate(row.original.tgl_berlaku, "DD MMMM YYYY"),
          },
          {
            width: 120,
            Header: "HET",
            accessor: "het",
            Cell: (row) => <label>Rp{formatNumber(row.original.het)},-</label>,
          },
          {
            Header: "Status",
            accessor: "status",
            width: 100,
            filterable: true,
            Filter: () => (
              <Form.Control
                as="select"
                name="status"
                value={values.status}
                onChange={(e) => {
                  handleChange(e);
                  setSearch(true);
                }}
              >
                <option value="">Semua Status</option>
                <option value="Aktif">Aktif</option>
                <option value="Tidak Aktif">Tidak Aktif</option>
              </Form.Control>
            ),
            Cell: (row) => {
              if (row.original.status === "Tidak Aktif") {
                return (
                  <div
                    style={{
                      backgroundColor: "red",
                      textAlign: "center",
                      color: "white",
                      cursor: "pointer",
                    }}
                  >
                    <label
                      style={{
                        cursor: "pointer",
                      }}
                    >
                      {row.original.status}
                    </label>
                  </div>
                );
              } else {
                return (
                  <div
                    style={{
                      backgroundColor: "green",
                      textAlign: "center",
                      color: "white",
                      cursor: "pointer",
                    }}
                  >
                    <label
                      style={{
                        cursor: "pointer",
                      }}
                    >
                      {row.original.status}
                    </label>
                  </div>
                );
              }
            },
          },
        ]}
      />
      <ModalTambahHet
        wilayah={wilayah}
        barang={dataBarang}
        isOpen={modalTambahHET}
        toggle={togleModalTambahHET}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    getHetIndex: (values) => dispatch(getHetIndex(values)),
    getBarangIndex: () => dispatch(getBarangIndex()),
    kotaBulogIndex: () => dispatch(kotaBulogIndex()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(HistoryDataTable);
