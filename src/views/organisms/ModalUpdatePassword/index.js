import React, { useState, useEffect } from "react";
import {
  Modal,
  ModalHeader,
  Button,
  Card,
  CardBody,
  ModalFooter,
  Alert,
  Input,
  FormGroup,
  Label,
  FormFeedback,
} from "reactstrap";

import { connect } from "react-redux";
import { fetchUpdatePassword } from "../../../redux/actions/auth";

const ModalUpdatePassword = ({ isOpen, toggle, user, fetchUpdatePassword }) => {
  const [data, setData] = useState({
    password: "",
    passwordLama: "",
    ulangipass: "",
  });
  const [alert, setAlert] = useState({
    alert: false,
    success: false,
    ulangipassValid: false,
    ulangipassInvalid: false,
  });
  const id = user.user?.id;

  const handleChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
    if (data.password !== "") {
      setAlert({ ulangipassInvalid: true });
    }
  };

  const handleSubmit = async () => {
    console.log(data.password, data.ulangipass);
    if (data.password === data.ulangipass) {
      const x = await fetchUpdatePassword(id, data);

      if (x.action.payload === "Password Salah") {
        setAlert({ alert: true });
        setData({ password: "", passwordLama: "", ulangipass: "" });
      } else {
        await setAlert({ success: true });
        setData({ password: "", passwordLama: "", ulangipass: "" });
        // console.log(x, "response");
        // window.location.reload();
      }
    } else {
      setAlert({ ulangipassInvalid: true });
    }
  };

  useEffect(() => {
    const x = () => {
      if (
        data.ulangipass === data.password ||
        data.password === data.ulangipass
      ) {
        setAlert({ ulangipassValid: true });
      } else {
        setAlert({ ulangipassInvalid: true });
      }
    };
    if (data.password !== "") x();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data.ulangipass]);

  return (
    <Modal
      isOpen={isOpen}
      toggle={toggle}
      backdrop="static"
      size="sm"
      keyboard={true}
    >
      <ModalHeader toggle={toggle}>Ubah Password</ModalHeader>
      <Card>
        <CardBody>
          {alert.success ? (
            <Alert color="success">Ubah Password Berhasil</Alert>
          ) : null}

          <FormGroup>
            <Label>Password Lama</Label>

            <Input
              name="passwordLama"
              invalid={alert.alert}
              placeholder="Masukkan Password Lama"
              type="password"
              value={data.passwordLama}
              onChange={(e) => handleChange(e)}
            />
            <FormFeedback>Password Salah</FormFeedback>
          </FormGroup>
          <FormGroup>
            <Label>Password Baru</Label>

            <Input
              name="password"
              value={data.password}
              placeholder="Masukkan Password Baru"
              type="password"
              onChange={(e) => handleChange(e)}
            />
          </FormGroup>
          <FormGroup>
            <Label>Ulangi Password Baru</Label>
            <Input
              valid={alert.ulangipassValid}
              invalid={alert.ulangipassInvalid}
              value={data.ulangipass}
              name="ulangipass"
              placeholder="Ulangi Password Baru"
              type="password"
              onChange={(e) => handleChange(e)}
            />
            <FormFeedback>Password Tidak Sama</FormFeedback>
          </FormGroup>
        </CardBody>
      </Card>
      <ModalFooter>
        <Button color="success" onClick={handleSubmit}>
          Ubah Password
        </Button>
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetchUpdatePassword: (id, data) => dispatch(fetchUpdatePassword(id, data)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalUpdatePassword);
