import React, { useState } from "react";
import { Modal, ModalHeader, ModalBody, Button, ModalFooter } from "reactstrap";
import { Table, Image } from "react-bootstrap";
import { formatNumber } from "../../../utils";
import { deleteRincian } from "../../../redux/actions/order";
import { connect } from "react-redux";
import load from "../../../assets/loading.svg";

const ModalDeleteRincian = (props) => {
  const {
    isOpen,
    toggle,
    deleteRincian,
    dataRincian,
    setModalDeleteRincian,
    modalDeleteRincian,
  } = props;
  const [loading, setLoading] = useState(false);

  const handleDeleteRincian = async (id) => {
    setLoading(true);
    const x = await deleteRincian(id);
    if (x) {
      setLoading(false);
      setModalDeleteRincian(!modalDeleteRincian);
    }
  };

  return (
    dataRincian && (
      <Modal
        isOpen={isOpen}
        toggle={toggle}
        backdrop="static"
        size="xl"
        keyboard={true}
      >
        <ModalHeader toggle={toggle}>Hapus Rincian Barang ?</ModalHeader>
        <ModalBody>
          <Table striped bordered hover responsive>
            <thead>
              <tr>
                <th>Nama Barang</th>
                <th>Unit</th>
                <th>Jumlah Barang</th>
                <th>Harga Per Satuan</th>
                <th>Total Harga</th>
                <th>HET</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{dataRincian.nama_barang}</td>
                <td>
                  {dataRincian.unit} {dataRincian.tipe_unit}
                </td>
                <td>{dataRincian.jumlah}</td>
                <td>Rp{formatNumber(dataRincian.harga_perunit)}</td>
                <td>Rp{formatNumber(dataRincian.total_harga)}</td>
                <td>Rp{formatNumber(dataRincian.het)}</td>
              </tr>
            </tbody>
          </Table>
        </ModalBody>
        <ModalFooter>
          <Button color="success" onClick={toggle}>
            Batal
          </Button>
          {loading ? (
            <Button color="primary" outline>
              <Image src={load} style={{ width: 20, height: 20 }} />
            </Button>
          ) : (
            <Button
              color="danger"
              onClick={() => handleDeleteRincian(dataRincian.id)}
            >
              Hapus
            </Button>
          )}
        </ModalFooter>
      </Modal>
    )
  );
};

const mapStateToProps = (state) => {
  return {
    list: state.list,
    user: state.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    deleteRincian: (id) => dispatch(deleteRincian(id)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ModalDeleteRincian);
