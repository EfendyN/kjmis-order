import React, { useState, useEffect } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Button,
  Card,
  CardBody,
  CardHeader,
  ModalFooter,
  CardFooter,
  Label,
} from "reactstrap";
import { Table, Image } from "react-bootstrap";
import { formatDate, formatNumber } from "../../../utils";
import { getRincianOrder } from "../../../redux/actions/list";
import {
  putOrder,
  updateRincian,
  deleteRincian,
} from "../../../redux/actions/order";
import { connect } from "react-redux";
import ModalUbahAsal from "../ModalEditOrder/ModalEditSuratAsal";
import ModalUbahTujuan from "../ModalEditOrder/ModalEditSuratTujuan";
import ModalDelete from "../ModalDelete";
import ModalEditRincian from "../ModalEditRincian";
import ModalDeleteRincian from "../ModalDeleteRincian";
import ModalTambahRincian from "../ModalTambahRincian";
import del from "../../../assets/cancel.png";
import edt from "../../../assets/file.png";
import add from "../../../assets/plus.svg";

const DetailModal = (props) => {
  const {
    isOpen,
    toggle,
    putOrder,
    activeData,
    getRincianOrder,
    user,
    updateRincian,
  } = props;
  const [next, setNext] = useState(false);
  const [bname, setBname] = useState("Rincian Barang");

  const [dataOrder, setDataOrder] = useState("");
  const [buttonLoading, setButtonLoading] = useState(false);
  const [modalDelete, setModalDelete] = useState(false);

  const [alert, setAlert] = useState({ alert: false });

  const [data, setData] = useState(null);
  const [dataRincian, setDataRincian] = useState(null);

  const [modalUbahAsal, setModalUbahAsal] = useState(false);
  const [modalUbahTujuan, setModalUbahTujuan] = useState(false);

  const [modalEditRincian, setModalEditRincian] = useState(false);
  const [modalDeleteRincian, setModalDeleteRincian] = useState(false);
  const [modalTambahRincian, setModalTambahRincian] = useState(false);

  const [dataEditRincian, setDataEditRincian] = useState(null);
  const [dataDeleteRincian, setDataDeleteRincian] = useState(null);

  const toggleUbahAsal = async () => {
    setDataOrder(data);
    setModalUbahAsal(!modalUbahAsal);
  };

  const toggleUbahTujuan = () => {
    setDataOrder(data);
    setModalUbahTujuan(!modalUbahTujuan);
  };

  const toggleModalDelete = () => {
    setModalDelete(!modalDelete);
  };

  const toggleModalEditRincian = (item) => {
    setModalEditRincian(!modalEditRincian);
    setDataEditRincian(item);
  };

  const toggleModalDeleteRincian = (item) => {
    setModalDeleteRincian(!modalDeleteRincian);
    setDataDeleteRincian(item);
  };
  const toggleModalTambahRincian = () => {
    setModalTambahRincian(!modalTambahRincian);
  };

  const handleChangeFormOrder = (e) => {
    setDataOrder({
      ...dataOrder,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmitEditSurat = async () => {
    // if (dataOrder?.daerah_tujuan === "") {
    //   setAlert({ alert: true });
    // } else {
    const id = dataOrder?.id;
    setButtonLoading(true);
    const x = await putOrder(id, dataOrder);
    if (x) {
      setButtonLoading(false);
      if (modalUbahAsal === true) setModalUbahAsal(false);
      if (modalUbahTujuan === true) setModalUbahTujuan(false);
    }
    // }
  };

  const handleEditRincian = async () => {
    if (
      dataEditRincian?.nama_barang === "" ||
      dataEditRincian?.unit === "" ||
      dataEditRincian?.jumlah === "" ||
      dataEditRincian?.harga_perunit === "" ||
      dataEditRincian?.het === ""
    ) {
      setAlert({ ...alert, alert: true });
    } else {
      setButtonLoading(true);
      const x = await updateRincian(dataEditRincian.id, dataEditRincian);
      if (x) {
        console.log(x);
        setButtonLoading(false);
        setModalEditRincian(false);
      }
    }
  };

  useEffect(() => {
    const x = async () => {
      const xx = await getRincianOrder(activeData.order.id);
      if (xx) {
        setData(xx.action.payload?.order);
        setDataRincian(xx.action.payload?.rincian);
      }
    };
    if (isOpen) x();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [toggle]);

  useEffect(() => {
    const x = async () => {
      const xx = await getRincianOrder(activeData.order.id);
      if (xx) {
        setData(xx.action.payload?.order);
      }
    };
    if (isOpen) x();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalUbahAsal]);

  useEffect(() => {
    const x = async () => {
      const xx = await getRincianOrder(activeData.order.id);
      if (xx) {
        setData(xx.action.payload?.order);
      }
    };
    if (isOpen) x();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalUbahTujuan]);

  useEffect(() => {
    const x = async () => {
      const xx = await getRincianOrder(activeData.order.id);
      if (xx) {
        setData(xx.action.payload?.order);
        setDataRincian(xx.action.payload?.rincian);
      }
    };
    if (isOpen) x();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalEditRincian]);

  useEffect(() => {
    const x = async () => {
      const xx = await getRincianOrder(activeData.id);
      if (xx) {
        setData(xx.action.payload?.order);
        setDataRincian(xx.action.payload?.rincian);
      }
    };
    if (isOpen) x();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalDeleteRincian]);

  useEffect(() => {
    const x = async () => {
      const xx = await getRincianOrder(activeData.id);
      if (xx) {
        setData(xx.action.payload?.order);
        setDataRincian(xx.action.payload?.rincian);
      }
    };
    if (isOpen) x();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalTambahRincian]);

  useEffect(() => {
    setNext(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [toggle]);

  return (
    <>
      <Modal
        isOpen={isOpen}
        toggle={toggle}
        backdrop="static"
        size={next ? "xl" : "md"}
        keyboard={true}
      >
        <ModalHeader toggle={toggle}>
          {next ? "Rincian Barang" : "Rincian Surat"}
        </ModalHeader>
        <ModalBody>
          {next ? (
            <>
              {user.user?.level === "1" || user.user?.level === "2" ? (
                <>
                  <Label>Tambah Barang</Label>{" "}
                  <Image
                    src={add}
                    style={{ cursor: "pointer", width: 30, height: 30 }}
                    onClick={() => toggleModalTambahRincian()}
                  />
                  <br />
                </>
              ) : null}
              <Table striped bordered hover responsive>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Barang</th>
                    <th>Unit</th>
                    <th>Jumlah Barang</th>
                    <th>Harga Per Satuan</th>
                    <th>Total Harga</th>
                    <th>HET</th>
                    {user.user?.level === "1" || user.user?.level === "2" ? (
                      <th>#</th>
                    ) : null}
                  </tr>
                </thead>
                <tbody>
                  {dataRincian.map((item, index) => (
                    <tr key={index}>
                      <th>{index + 1}</th>
                      <td>{item.nama_barang}</td>
                      <td>
                        {item.unit} {item.tipe_unit}
                      </td>
                      <td>{item.jumlah}</td>
                      <td>Rp{formatNumber(item.harga_perunit)}</td>
                      <td>Rp{formatNumber(item.total_harga)}</td>
                      <td>Rp{formatNumber(item.het)}</td>
                      {user.user?.level === "1" || user.user?.level === "2" ? (
                        <td>
                          <Image
                            src={edt}
                            style={{
                              cursor: "pointer",
                              width: 30,
                              height: 30,
                              marginRight: 10,
                            }}
                            onClick={() => toggleModalEditRincian(item)}
                          />
                          <Image
                            src={del}
                            style={{ cursor: "pointer", width: 30, height: 30 }}
                            onClick={() => toggleModalDeleteRincian(item)}
                          />
                        </td>
                      ) : null}
                    </tr>
                  ))}
                </tbody>
              </Table>
            </>
          ) : (
            <>
              <Card>
                <CardHeader>
                  <strong>Surat Asal</strong>
                </CardHeader>
                <CardBody>
                  <Table bordered>
                    {data && (
                      <tbody>
                        <tr>
                          <td>Asal Pesanan</td>
                          <td>{data.asal_pesanan}</td>
                        </tr>
                        <tr>
                          <td>No Surat Pesanan</td>
                          <td>{data.no_surat_pesanan}</td>
                        </tr>
                        <tr>
                          <td>Tanggal Surat Pesanan</td>
                          <td>
                            {formatDate(data.tgl_surat_pesanan, "DD MMMM YYYY")}
                          </td>
                        </tr>
                      </tbody>
                    )}
                  </Table>
                </CardBody>
                {user.user?.level === "1" || user.user?.level === "2" ? (
                  <CardFooter>
                    <Button color="success" onClick={() => toggleUbahAsal()}>
                      Ubah
                    </Button>
                  </CardFooter>
                ) : null}
              </Card>
              <br />
              <Card>
                <CardHeader>
                  <strong>Surat Tujuan</strong>
                </CardHeader>
                <CardBody>
                  <Table bordered>
                    {data && (
                      <tbody>
                        <tr>
                          <td>Provinsi</td>
                          <td>{data.tujuan_provinsi}</td>
                        </tr>
                        <tr>
                          <td>Supplier</td>
                          <td>{data.tujuan_supplier}</td>
                        </tr>
                        <tr>
                          <td>Kantor Supplier</td>
                          <td>{data.daerah_tujuan_bulog}</td>
                        </tr>
                        <tr>
                          <td>No Surat</td>
                          <td>{data.no_surat_tujuan}</td>
                        </tr>
                        <tr>
                          <td>Tanggal Surat</td>
                          <td>
                            {formatDate(data.tgl_surat_tujuan, "DD MMMM YYYY")}
                          </td>
                        </tr>
                        <tr>
                          <td>Daerah Tujuan</td>
                          <td>{data.daerah_tujuan}</td>
                        </tr>
                      </tbody>
                    )}
                  </Table>
                </CardBody>{" "}
                {user.user?.level === "1" || user.user?.level === "2" ? (
                  <CardFooter>
                    <Button color="success" onClick={toggleUbahTujuan}>
                      Ubah
                    </Button>
                  </CardFooter>
                ) : null}
              </Card>
              <br />
              <Card>
                <CardHeader>
                  <strong>Rincian Pemesanan</strong>
                </CardHeader>
                <CardBody>
                  <Table bordered>
                    {data && (
                      <tbody>
                        <tr>
                          <td>Tanggal Pengambilan</td>
                          <td>
                            {formatDate(data.tgl_pengambilan, "DD MMMM YYYY")}
                          </td>
                        </tr>
                        <tr>
                          <td>Status</td>
                          {data.status === "Lunas" ? (
                            <td
                              style={{
                                backgroundColor: "green",
                                color: "white",
                                textAlign: "center",
                              }}
                            >
                              {data.status}
                            </td>
                          ) : (
                            <td
                              style={{
                                backgroundColor: "red",
                                color: "white",
                                textAlign: "center",
                              }}
                            >
                              {data.status}
                            </td>
                          )}
                        </tr>
                      </tbody>
                    )}
                  </Table>
                </CardBody>
              </Card>
            </>
          )}
          <ModalFooter>
            {user.user?.level === "1" ? (
              <Button color="danger" onClick={toggleModalDelete}>
                Hapus Pesanan
              </Button>
            ) : null}
            <Button
              color="success"
              onClick={() => {
                setNext(!next);
                setBname(
                  bname === "Rincian Barang"
                    ? "Rincian Surat"
                    : "Rincian Barang"
                );
              }}
            >
              {bname}
            </Button>
          </ModalFooter>
        </ModalBody>
      </Modal>
      <ModalUbahAsal
        isOpen={modalUbahAsal}
        toggle={toggleUbahAsal}
        alert={alert}
        dataOrder={dataOrder}
        handleChangeFormOrder={handleChangeFormOrder}
        handleSubmitEditSurat={handleSubmitEditSurat}
        buttonLoading={buttonLoading}
      />
      <ModalUbahTujuan
        isOpen={modalUbahTujuan}
        toggle={toggleUbahTujuan}
        alert={alert}
        dataOrder={dataOrder}
        handleChangeFormOrder={handleChangeFormOrder}
        handleSubmitEditSurat={handleSubmitEditSurat}
        buttonLoading={buttonLoading}
        setDataOrder={setDataOrder}
      />
      <ModalDelete
        isOpen={modalDelete}
        toggle={toggleModalDelete}
        data={data?.id}
      />
      <ModalEditRincian
        isOpen={modalEditRincian}
        toggle={toggleModalEditRincian}
        data={dataEditRincian}
        setData={setDataEditRincian}
        handleEditRincian={handleEditRincian}
        buttonLoading={buttonLoading}
        setAlert={setAlert}
        provinsi={data?.tujuan_provinsi}
        alert={alert}
      />
      <ModalDeleteRincian
        toggle={toggleModalDeleteRincian}
        isOpen={modalDeleteRincian}
        dataRincian={dataDeleteRincian}
        modalDeleteRincian={modalDeleteRincian}
        setModalDeleteRincian={setModalDeleteRincian}
      />
      <ModalTambahRincian
        toggle={toggleModalTambahRincian}
        isOpen={modalTambahRincian}
        toggleModalTambahRincian={toggleModalTambahRincian}
        provinsi={data?.tujuan_provinsi}
        orderid={data?.id}
        alert={alert}
        setAlert={setAlert}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    list: state.list,
    user: state.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getRincianOrder: (id) => dispatch(getRincianOrder(id)),
    putOrder: (id, dataOrder) => dispatch(putOrder(id, dataOrder)),
    deleteRincian: (id) => dispatch(deleteRincian(id)),
    updateRincian: (id, dataRincian) =>
      dispatch(updateRincian(id, dataRincian)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DetailModal);
