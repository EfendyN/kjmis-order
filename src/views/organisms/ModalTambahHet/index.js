import React, { useState, useEffect } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Table,
  Button,
  Card,
  CardBody,
  ModalFooter,
  Alert,
  Input,
  FormGroup,
  Label,
  Col,
  FormFeedback,
} from "reactstrap";

import { Form } from "react-bootstrap";

import { formatNumber, formatDate } from "../../../utils";
import { connect } from "react-redux";
import { postHet } from "../../../redux/actions/barang";

const ModalUpdatePassword = ({ isOpen, toggle, wilayah, barang, postHet }) => {
  const [values, setValues] = useState({
    nama_barang: "",
    wilayah: "",
    het: null,
  });

  const [alert, setAlert] = useState({
    alert: false,
    success: false,
    nama_barang: false,
    wilayah: false,
    het: false,
  });
  const [buttonLoading, setButtonLoading] = useState(false);
  const [newHet, setNewHet] = useState({});
  const [oldHet, setOldHet] = useState({});
  const [modalRincian, setModalRincian] = useState(false);

  useEffect(() => {
    setAlert({
      alert: false,
      success: false,
      nama_barang: false,
      wilayah: false,
      het: false,
    });
    setValues({ nama_barang: "", wilayah: "", het: null });
  }, [isOpen]);

  const toggleModalRincian = () => {
    setModalRincian(!modalRincian);
  };
  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
    setAlert({
      alert: false,
      success: false,
      nama_barang: false,
      wilayah: false,
      het: false,
    });
  };

  const handleSubmit = async () => {
    if (values.nama_barang === "") {
      setAlert({ ...alert, nama_barang: true });
    } else if (values.wilayah === "") {
      setAlert({ ...alert, wilayah: true });
    } else if (values.het === null || values.het === "") {
      setAlert({ ...alert, het: true });
    } else {
      setButtonLoading(true);
      const x = await postHet(values);
      if (x) {
        // console.log({ x });
        setNewHet(x.action.payload.newHet);
        setOldHet(x.action.payload.oldHet);
        toggleModalRincian();
        setButtonLoading(false);
        setAlert({ ...alert, success: true });
        setValues({ nama_barang: "", wilayah: "", het: null });
      }
    }
  };
  return (
    <>
      <Modal
        isOpen={isOpen}
        toggle={toggle}
        backdrop="static"
        size="md"
        keyboard={true}
      >
        <ModalHeader toggle={toggle}>Tambah HET</ModalHeader>
        <Card>
          <CardBody>
            {alert.success ? (
              <Alert color="success">HET Berhasil Ditambahkan</Alert>
            ) : null}

            <FormGroup row>
              <Label sm={4}>Nama Barang</Label>
              <Col sm={8}>
                <Form.Group>
                  <Form.Control
                    as="select"
                    name="nama_barang"
                    isInvalid={alert.nama_barang}
                    value={values.nama_barang}
                    onChange={(e) => {
                      handleChange(e);
                    }}
                  >
                    <option value="">Pilih Barang</option>
                    {barang.map((item, index) => (
                      <option key={index}>{item.nama}</option>
                    ))}
                  </Form.Control>
                  <Form.Control.Feedback type="invalid">
                    Pilih Barang Terlebih Dahulu
                  </Form.Control.Feedback>
                </Form.Group>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Wilayah</Label>
              <Col sm={8}>
                <Form.Group>
                  <Form.Control
                    as="select"
                    name="wilayah"
                    isInvalid={alert.wilayah}
                    value={values.wilayah}
                    onChange={(e) => {
                      handleChange(e);
                    }}
                  >
                    <option value="">Pilih Wilayah</option>
                    {wilayah.map((item, index) => (
                      <option key={index}>{item.provinsi}</option>
                    ))}
                  </Form.Control>
                  <Form.Control.Feedback type="invalid">
                    Pilih Wilayah Terlebih Dahulu
                  </Form.Control.Feedback>
                </Form.Group>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>HET (Rp)</Label>
              <Col sm={8}>
                <Input
                  name="het"
                  value={values.het || ""}
                  invalid={alert.het}
                  onChange={(e) => handleChange(e)}
                  type="number"
                />
                <FormFeedback>Masukkan HET</FormFeedback>
              </Col>
            </FormGroup>
          </CardBody>
        </Card>
        <ModalFooter>
          {buttonLoading ? (
            <Button color="success">Loading...</Button>
          ) : (
            <Button color="success" onClick={handleSubmit}>
              Tambah HET
            </Button>
          )}
        </ModalFooter>
      </Modal>

      {newHet && (
        <Modal size="lg" isOpen={modalRincian}>
          <ModalHeader toggle={toggleModalRincian}>
            Rincian Penambahan HET
          </ModalHeader>
          <ModalBody>
            <Table responsive bordered>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nama Barang</th>
                  <th>Wilayah</th>
                  <th>Tanggal Penambahan HET</th>
                  <th>HET</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Het Baru</td>
                  <td>{newHet.nama_barang}</td>
                  <td>{newHet.wilayah}</td>
                  <td>{formatDate(newHet.tgl_berlaku, "DD MMMM YYYY")}</td>
                  <td>Rp{formatNumber(newHet.het)},-</td>
                  <td>
                    <div
                      style={{
                        backgroundColor: "green",
                        textAlign: "center",
                        color: "white",
                      }}
                    >
                      {newHet.status}
                    </div>
                  </td>
                </tr>
                {oldHet && (
                  <tr>
                    <td>Het Lama</td>
                    <td>{oldHet.nama_barang}</td>
                    <td>{oldHet.wilayah}</td>
                    <td>{formatDate(oldHet.tgl_berlaku, "DD MMMM YYYY")}</td>
                    <td>Rp{formatNumber(oldHet.het)},-</td>
                    <td>
                      <div
                        style={{
                          backgroundColor: "red",
                          textAlign: "center",
                          color: "white",
                        }}
                      >
                        {oldHet.status}
                      </div>
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </ModalBody>
        </Modal>
      )}
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    postHet: (values) => dispatch(postHet(values)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalUpdatePassword);
