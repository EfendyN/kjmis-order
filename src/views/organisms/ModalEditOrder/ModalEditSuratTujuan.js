import React, { useState, useEffect } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Col,
  Button,
  FormGroup,
  Label,
  Input,
  Card,
  CardBody,
  CardHeader,
} from "reactstrap";
import { Form, Image } from "react-bootstrap";
// import rincianBarang from "../../../constans/imputRincian";
import { connect } from "react-redux";
import {
  provinsiIndex,
  kotaIndex,
  kotaBulogIndex,
} from "../../../redux/actions/list";
import moment from "moment";
import loading from "../../../assets/loading.svg";

const ModalEditSuratTujuan = ({
  isOpen,
  toggle,
  kotaIndex,
  kotaBulogIndex,
  dataOrder,
  setDataOrder,
  handleChangeFormOrder,
  handleSubmitEditSurat,
  buttonLoading,
}) => {
  const [kota, setKota] = useState({ data: [] });
  const [kotaBulog, setKotaBulog] = useState({ data: [] });
  const [provinsi, setProvinsi] = useState("");
  const [forms, setForms] = useState(true);

  const [minTglTujuan, setMinTglTujuan] = useState("");

  useEffect(() => {
    const dataKota = async () => {
      const res = await kotaIndex(provinsi);
      setKota({ data: res.action.payload.provinsi });
    };
    const dataKotaBulog = async () => {
      const res = await kotaBulogIndex();
      setKotaBulog({ data: res.action.payload });
    };
    if (isOpen) {
      dataKota();
      dataKotaBulog();
    }
    if (dataOrder.tujuan_supplier === "BULOG") {
      setForms(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isOpen]);

  useEffect(() => {
    const dataKota = async () => {
      const res = await kotaIndex(provinsi);
      setKota({ data: res.action.payload.provinsi });
    };
    dataKota();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [provinsi]);

  useEffect(() => {
    const tgl = async () => {
      setDataOrder({
        ...dataOrder,
        tgl_pengambilan: moment(dataOrder.tgl_surat_tujuan)
          .add(1, "day")
          .format("YYYY-MM-DD")
          .toString(),
      });
    };
    tgl();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataOrder?.tgl_surat_tujuan]);

  useEffect(() => {
    const tgl = async () => {
      setMinTglTujuan(dataOrder?.tgl_surat_pesanan);
    };
    tgl();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataOrder?.tgl_surat_pesanan]);

  const handleChangeKota = async (e) => {
    setProvinsi(e.target.value);
  };

  const handleChangeForm = (e) => {
    if (e === "BULOG") {
      setDataOrder({
        ...dataOrder,
        tujuan_supplier: "BULOG",
        // daerah_tujuan_bulog: "",
      });
      setForms(false);
    } else {
      setDataOrder({
        ...dataOrder,
        tujuan_supplier: "",
        // daerah_tujuan_bulog: "",
      });
      setForms(true);
    }
  };

  return (
    dataOrder && (
      <Modal
        isOpen={isOpen}
        toggle={toggle}
        backdrop="static"
        size="md"
        keyboard={true}
      >
        <ModalHeader toggle={toggle}>Ubah Rincian Surat Tujuan</ModalHeader>
        <ModalBody>
          <Label>Pilih Supplier</Label>
          <FormGroup row>
            <Col sm={4}>
              <FormGroup check>
                <Label check>
                  <Input
                    type="radio"
                    name="radio1"
                    onClick={(e) => {
                      handleChangeForm("BULOG");
                    }}
                    defaultChecked={
                      dataOrder.tujuan_supplier === "BULOG" ? true : false
                    }
                  />
                  Bulog
                </Label>
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup check>
                <Label check>
                  <Input
                    type="radio"
                    name="radio1"
                    onClick={(e) => {
                      handleChangeForm("x");
                    }}
                    defaultChecked={
                      dataOrder.tujuan_supplier !== "BULOG" ? true : false
                    }
                  />
                  Lainnya
                </Label>
              </FormGroup>
            </Col>
          </FormGroup>
          <Form>
            <Card>
              <CardHeader>
                <strong>Tujuan Pesanan</strong>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Label sm={6}>Provinsi</Label>
                  <Col sm={6}>
                    <Form.Control
                      as="select"
                      name="tujuan_provinsi"
                      value={dataOrder.tujuan_provinsi}
                      onChange={async (e) => {
                        handleChangeFormOrder(e);
                        handleChangeKota(e);
                      }}
                    >
                      <option>Pilih Provinsi</option>
                      {kotaBulog.data.map((item, index) => (
                        <option key={index}>{item.provinsi}</option>
                      ))}
                    </Form.Control>
                  </Col>
                </FormGroup>
                {forms ? (
                  <>
                    <FormGroup row>
                      <Label sm={6}>Supplier</Label>
                      <Col sm={6}>
                        <Input
                          name="tujuan_supplier"
                          value={dataOrder.tujuan_supplier}
                          onChange={(e) => handleChangeFormOrder(e)}
                          placeholder="Supplier"
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Label sm={6}>Kantor Supplier</Label>
                      <Col sm={6}>
                        <Form.Control
                          as="select"
                          name="daerah_tujuan_bulog"
                          value={dataOrder.daerah_tujuan_bulog}
                          onChange={(e) => handleChangeFormOrder(e)}
                        >
                          <option>Pilih Kota/Kabupaten</option>
                          {kota.data.map((item, index) => (
                            <option key={index}>{item.wil_kab}</option>
                          ))}
                        </Form.Control>
                      </Col>
                    </FormGroup>
                  </>
                ) : null}

                <FormGroup row>
                  <Label sm={6}>Untuk Daerah</Label>
                  <Col sm={6}>
                    <Form.Control
                      as="select"
                      name="daerah_tujuan"
                      value={dataOrder.daerah_tujuan}
                      onChange={(e) => handleChangeFormOrder(e)}
                    >
                      <option>Pilih Kota/Kabupaten</option>
                      {kota.data.map((item, index) => (
                        <option key={index}>{item.wil_kab}</option>
                      ))}
                    </Form.Control>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={6}>No Surat</Label>
                  <Col sm={6}>
                    <Input
                      name="no_surat_tujuan"
                      value={dataOrder.no_surat_tujuan}
                      onChange={(e) => handleChangeFormOrder(e)}
                      placeholder="Nomor Surat"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={6}>Tanggal Surat</Label>
                  <Col sm={6}>
                    <Input
                      type="date"
                      name="tgl_surat_tujuan"
                      min={minTglTujuan}
                      value={dataOrder.tgl_surat_tujuan}
                      onChange={(e) => handleChangeFormOrder(e)}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={6}>Tanggal Pengambilan</Label>
                  <Col sm={6}>
                    <div style={{ textAlign: "center" }}>
                      <label>
                        {dataOrder.tgl_pengambilan === "Invalid date"
                          ? null
                          : moment(dataOrder.tgl_pengambilan)
                              .format("DD-MM-YYYY")
                              .toString()}
                      </label>
                    </div>
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
            <br />
            {/* {alert.alert ? (
              <Alert color="danger">Harap isi Form yang kosong</Alert>
            ) : null} */}
          </Form>
        </ModalBody>
        <ModalFooter>
          {buttonLoading ? (
            <Button color="primary" outline>
              <Image src={loading} style={{ width: 20, height: 20 }} />
            </Button>
          ) : (
            <Button color="success" onClick={handleSubmitEditSurat}>
              Selesai
            </Button>
          )}
        </ModalFooter>
      </Modal>
    )
  );
};
const mapDispatchToProps = (dispatch) => {
  return {
    provinsiIndex: () => dispatch(provinsiIndex()),
    kotaBulogIndex: () => dispatch(kotaBulogIndex()),
    kotaIndex: (provinsi) => dispatch(kotaIndex(provinsi)),
  };
};
export default connect(null, mapDispatchToProps)(ModalEditSuratTujuan);
