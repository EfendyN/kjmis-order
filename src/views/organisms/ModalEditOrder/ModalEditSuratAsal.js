import React from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Col,
  Button,
  FormGroup,
  Label,
  Input,
  Card,
  CardBody,
  CardHeader,
} from "reactstrap";
import { Form, Image } from "react-bootstrap";
import loading from "../../../assets/loading.svg";

const ModalEditSuratAsal = ({
  isOpen,
  toggle,
  data,
  handleChangeFormOrder,
  dataOrder,
  buttonLoading,
  alert,
  handleSubmitEditSurat,
}) => {
  return (
    dataOrder && (
      <Modal
        isOpen={isOpen}
        toggle={toggle}
        backdrop="static"
        size="md"
        keyboard={true}
      >
        <ModalHeader toggle={toggle}>Ubah Rincial Surat Asal</ModalHeader>
        <ModalBody>
          <Form>
            <Card>
              <CardHeader>
                <strong sm={6}>Asal Pesanan</strong>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Label sm={6}>Asal Pesanan</Label>
                  <Col sm={6}>
                    <Input
                      name="asal_pesanan"
                      value={dataOrder.asal_pesanan}
                      onChange={(e) => handleChangeFormOrder(e)}
                      placeholder="Asal Pesanan"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={6}>Nomor Pesanan</Label>
                  <Col sm={6}>
                    <Input
                      name="no_surat_pesanan"
                      value={dataOrder.no_surat_pesanan}
                      placeholder="Nomor Surat"
                      onChange={(e) => handleChangeFormOrder(e)}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={6}>Tanggal Pemesanan</Label>
                  <Col sm={6}>
                    <Input
                      name="tgl_surat_pesanan"
                      type="date"
                      value={dataOrder.tgl_surat_pesanan}
                      onChange={(e) => handleChangeFormOrder(e)}
                    />
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
            {/* <Alert color="danger">Harap isi Form yang kosong</Alert> */}
          </Form>
        </ModalBody>
        <ModalFooter>
          {buttonLoading ? (
            <Button color="primary" outline>
              <Image src={loading} style={{ width: 20, height: 20 }} />
            </Button>
          ) : (
            <Button color="success" onClick={handleSubmitEditSurat}>
              Selesai
            </Button>
          )}
        </ModalFooter>
      </Modal>
    )
  );
};

export default ModalEditSuratAsal;
