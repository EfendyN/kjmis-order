import React, { useState, useEffect } from "react";
import { Navbar, Image, Nav, NavDropdown } from "react-bootstrap";
import { Button } from "reactstrap";
import { connect } from "react-redux";
import sjs from "../../assets/sjs.jpg";
import kjmis from "../../assets/kjmis.jpg";
import userIcon from "../../assets/user.png";
import { Link } from "react-router-dom";

import ModalUpdatePassword from "../organisms/ModalUpdatePassword";

import { getUser } from "../../redux/actions/user";

const NavBar = ({ mode, getUser, user, toggleModalTambahPemesanan }) => {
  useEffect(() => {
    getUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const [modal, setModal] = useState(false);
  const togleModal = () => {
    setModal(!modal);
  };

  const navDropdownTitle = (
    <>
      <label style={{ fontSize: 20 }}>{user.user?.username}</label>{" "}
      <Image src={userIcon} className="logo" />
    </>
  );
  return (
    <>
      <Navbar bg="light" variant="light">
        <Link to="/home">
          <Navbar.Brand>
            <Image src={kjmis} className="logo" />
            <Image src={sjs} className="logo" />
          </Navbar.Brand>
        </Link>
        <Nav>
          <strong>{mode}</strong>
        </Nav>
        <Navbar.Collapse
          id="responsive-navbar-nav"
          className="justify-content-end"
        >
          <Nav>
            {mode === "DATA PESANAN" ? (
              <>
                {user.user?.level === "1" || user.user?.level === "2" ? (
                  <>
                    <Button
                      outline
                      color="primary"
                      onClick={toggleModalTambahPemesanan}
                      className="nav-button"
                    >
                      Buat Pesanan
                    </Button>
                    <Link to="/history">
                      <Button className="nav-button" outline color="primary">
                        Riwayat
                      </Button>
                    </Link>
                    <Link to="/data">
                      <Button className="nav-button" outline color="primary">
                        Data
                      </Button>
                    </Link>
                  </>
                ) : null}
              </>
            ) : mode === "RIWAYAT PESANAN" ? (
              <div className="floatright">
                <Link to="/home">
                  <Button className="nav-button" outline color="primary">
                    Data Pesanan
                  </Button>
                </Link>
                <Link to="/data">
                  <Button className="nav-button" outline color="primary">
                    Data
                  </Button>
                </Link>
              </div>
            ) : mode === "DATA" ? (
              <div className="floatright">
                <Link to="/home">
                  <Button className="nav-button" outline color="primary">
                    Data Pesanan
                  </Button>
                </Link>{" "}
                <Link to="/history">
                  <Button className="nav-button" outline color="primary">
                    Riwayat
                  </Button>
                </Link>
              </div>
            ) : null}
          </Nav>
          <div style={{ marginRight: 30 }}></div>
          <Nav>
            <NavDropdown title={navDropdownTitle}>
              <NavDropdown.Item
                onClick={() => {
                  setModal(true);
                }}
              >
                Ubah Password
              </NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item
                onClick={() => {
                  localStorage.clear();
                  window.location.reload();
                }}
              >
                Keluar
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
        <div style={{ marginRight: 50 }}></div>
      </Navbar>
      <ModalUpdatePassword toggle={togleModal} isOpen={modal} />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getUser: () => dispatch(getUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
