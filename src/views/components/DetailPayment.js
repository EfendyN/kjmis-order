import React from "react";
import {
  Table,
  Card,
  CardBody,
  CardHeader,
} from "reactstrap";
import { formatDate } from "../../utils";

const DetailPayment = (props) => {
  const { stateRincian } = props;

  return (
    <>
      <Card>
        <CardHeader>
          <strong>Asal Surat</strong>
        </CardHeader>
        <CardBody>
          <Table bordered responsive>
            <tbody>
              <tr>
                <td>Asal Pesanan</td>
                <td>{stateRincian.order?.asal_pesanan}</td>
              </tr>
              <tr>
                <td>No Surat</td>
                <td>{stateRincian.order?.no_surat_pesanan}</td>
              </tr>
              <tr>
                <td>Tanggal Surat</td>
                <td>
                  {formatDate(
                    stateRincian.order?.tgl_surat_pesanan,
                    "DD MMMM YYYY"
                  )}
                </td>
              </tr>
            </tbody>
          </Table>
        </CardBody>
      </Card>
      <br />
      <Card>
        <CardHeader>
          <strong>Surat Tujuan</strong>
        </CardHeader>
        <CardBody>
          <Table bordered responsive>
            <tbody>
              <tr>
                <td>Tujuan Pesanan (Provinsi)</td>
                <td>{stateRincian.order?.tujuan_provinsi}</td>
              </tr>
              <tr>
                <td>Untuk Daerah</td>
                <td>{stateRincian.order?.daerah_tujuan}</td>
              </tr>
              <tr>
                <td>No Surat</td>
                <td>{stateRincian.order?.no_surat_tujuan}</td>
              </tr>
              <tr>
                <td>Tanggal Surat</td>
                <td>
                  {formatDate(
                    stateRincian.order?.tgl_surat_tujuan,
                    "DD MMMM YYYY"
                  )}
                </td>
              </tr>
            </tbody>
          </Table>
        </CardBody>
      </Card>
    </>
  );
};

export default DetailPayment;
