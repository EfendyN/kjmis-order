import React from "react";
import {
  Table,
  Card,
  CardBody,
  CardHeader,
  Row,
  Col,
  CardFooter,
  Button,
} from "reactstrap";
import { formatDate, formatNumber } from "../../utils";
import { connect } from "react-redux";

const DetailPaymentTagihan = ({
  stateRincian,
  setMode,
  setModalEdit,
  user,
  setLogMode,
  toggleModalLog,
}) => {
  return (
    <>
      <Row>
        <Col>
          <Card>
            <CardHeader>
              <strong>Rincian Pembayaran</strong>
            </CardHeader>
            <CardBody>
              <Table>
                <tbody>
                  <tr>
                    <td>Tagihan</td>
                    <td>
                      <label>
                        Rp
                        {formatNumber(stateRincian.payment?.tagihan_bulog)}
                        ,-
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>Tanggal Bayar</td>
                    <td>
                      <label>
                        {stateRincian.payment?.tgl_bayar_bulog
                          ? formatDate(
                              stateRincian.payment?.tgl_bayar_bulog,
                              "DD MMMM YYYY"
                            )
                          : "-"}
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>Bayar Senilai (Rp)</td>
                    <td>
                      <label>
                        Rp
                        {formatNumber(stateRincian.payment?.bayar_bulog)}
                        ,-
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>Balance (Rp)</td>
                    <td>
                      <label>
                        {stateRincian.payment?.balance_bulog
                          ? "Rp" +
                            formatNumber(stateRincian.payment?.balance_bulog) +
                            ",-"
                          : "-"}
                      </label>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </CardBody>
            {user.user.level === "1" ? (
              <CardFooter>
                <Button
                  color="success"
                  onClick={() => {
                    setModalEdit(true);
                    setMode("Pembayaran");
                  }}
                >
                  Ubah
                </Button>{" "}
                <Button
                  color="success"
                  onClick={() => {
                    toggleModalLog();
                    setLogMode("OUT");
                  }}
                >
                  Riwayat
                </Button>
              </CardFooter>
            ) : null}
          </Card>
        </Col>
        <br />
        <Col>
          <Card>
            <CardHeader>
              <strong>Rincian Terima Pembayaran</strong>
            </CardHeader>
            <CardBody>
              <Table>
                <tbody>
                  <tr>
                    <td>Tagihan</td>
                    <td>
                      <label>
                        Rp
                        {formatNumber(stateRincian.payment?.tagihan_terima)}
                        ,-
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>Tanggal Terima</td>
                    <td>
                      <label>
                        {stateRincian.payment?.tgl_terima_bayar
                          ? formatDate(
                              stateRincian.payment?.tgl_terima_bayar,
                              "DD MMMM YYYY"
                            )
                          : "-"}
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>Terima Senilai (Rp)</td>
                    <td>
                      <label>
                        Rp
                        {formatNumber(stateRincian.payment?.terima_bayar)}
                        ,-
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>Balance (Rp)</td>
                    <td>
                      <label>
                        {stateRincian.payment?.balance_terima
                          ? "Rp" +
                            formatNumber(stateRincian.payment?.balance_terima) +
                            ",-"
                          : "-"}
                      </label>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </CardBody>
            {user.user.level === "1" ? (
              <CardFooter>
                <Button
                  color="success"
                  onClick={() => {
                    setModalEdit(true);
                    setMode("Terima");
                  }}
                >
                  Ubah
                </Button>{" "}
                <Button
                  color="success"
                  onClick={() => {
                    toggleModalLog();
                    setLogMode("IN");
                  }}
                >
                  Riwayat
                </Button>
              </CardFooter>
            ) : null}
          </Card>
        </Col>
      </Row>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};

export default connect(mapStateToProps, null)(DetailPaymentTagihan);
