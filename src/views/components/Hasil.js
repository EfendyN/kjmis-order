import React from "react";
import { Row, Col } from "reactstrap";
import { formatNumber } from "../../utils";

const Hasil = ({ data, values }) => {
  return (
    <Row>
      <Col>
        {/* <label>Nama Barang</label>
        <br />
        <label>Provinsi</label>
        <br />
        <label>Total Unit</label>
        <br /> */}
        <label>Total Penerimaan (Rp)</label>
        <br />
        <label>Total Pembayaran (Rp)</label>
        <br />
      </Col>
      <Col>
        {/* <label>: {values.nama_barang}</label>
        <br />
        <label>: {values.provinsi}</label>
        <br />
        <label>
          : {data.total_unit} {values.tipe_unit}
        </label>
        <br /> */}
        <label>: Rp{formatNumber(data.total_penerimaan)},-</label>
        <br />
        <label>: Rp{formatNumber(data.total_pembayaran)},-</label>
        <br />
      </Col>
    </Row>
  );
};

export default Hasil;
