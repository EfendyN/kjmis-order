import React from "react";

import { Col, FormGroup, Label, Input, Row, Alert } from "reactstrap";

import { Table } from "react-bootstrap";

import { Image } from "react-bootstrap";

import RincianBarang from "./RincianBarang";

import plus from "../../assets/plus.svg";

import { Form } from "react-bootstrap";
const FormRincianBarang = ({
  handleChangeRincian,
  rincian,
  barang,
  valueRincian,
  buttonTambah,
  deleteRincian,
  alertR,
}) => {
  return (
    <>
      {alertR ? (
        <Alert color="danger">Tambahkan Barang Terlebih Daluhu</Alert>
      ) : null}
      <Table striped bordered hover>
        <thead>
          <tr>
            <th width="5%">#</th>
            <th>Nama Barang</th>
            <th>Unit</th>
            <th>Jumlah</th>
            <th>Harga Per Satuan</th>
            <th>HET</th>
            <th width="5%"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td width="5%"></td>
            <td width="20%">
              <FormGroup>
                <Form.Control
                  as="select"
                  name="nama_barang"
                  value={valueRincian.nama_barang}
                  onChange={(e) => handleChangeRincian(e)}
                >
                  <option>Pilih Barang</option>
                  {barang.data.map((item, index) => (
                    <option key={index}>{item.nama}</option>
                  ))}
                </Form.Control>
              </FormGroup>
            </td>
            <td width="15%">
              <Row>
                <Col sm={8}>
                  <Input
                    name="unit"
                    type="number"
                    value={valueRincian.unit}
                    onChange={(e) => {
                      handleChangeRincian(e);
                    }}
                  />
                </Col>
                <Col sm={2}>
                  <label>{valueRincian.tipe_unit}</label>
                </Col>
              </Row>
            </td>
            <td width="10%">
              <FormGroup>
                <Input
                  name="jumlah"
                  type="number"
                  value={valueRincian.jumlah}
                  onChange={(e) => handleChangeRincian(e)}
                />
              </FormGroup>
            </td>
            <td>
              <FormGroup>
                <Row>
                  <Col sm={1}>
                    <Label>Rp</Label>
                  </Col>
                  <Col sm={10}>
                    <Input
                      name="harga_perunit"
                      type="number"
                      value={valueRincian.harga_perunit}
                      onChange={(e) => handleChangeRincian(e)}
                    />
                  </Col>
                </Row>
              </FormGroup>
            </td>
            <td>
              <FormGroup>
                <Row>
                  <Col sm={1}>
                    <Label>Rp</Label>
                  </Col>
                  <Col sm={10}>
                    <Input
                      name="het"
                      type="number"
                      value={valueRincian.het}
                      onChange={(e) => handleChangeRincian(e)}
                    />
                  </Col>
                </Row>
              </FormGroup>
            </td>
            <td width="5%">
              <Image
                src={plus}
                style={{ cursor: "pointer", width: 30, height: 30 }}
                onClick={buttonTambah}
              />
            </td>
          </tr>
        </tbody>
      </Table>

      <RincianBarang rincian={rincian} deleteRincian={deleteRincian} />
    </>
  );
};

export default FormRincianBarang;
