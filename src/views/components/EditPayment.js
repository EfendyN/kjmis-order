import React, { useState, useEffect } from "react";
import {
  Table,
  Card,
  CardBody,
  CardHeader,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Alert,
} from "reactstrap";
import { formatNumber } from "../../utils";
import loading from "../../assets/loading.svg";
import { Image } from "react-bootstrap";
const EditPayment = ({
  stateRincian,
  pay,
  handleChange,
  data,
  modalEdit,
  toggleEdit,
  handleSubmit,
  buttonLoading,
  mode,
}) => {
  const [alert, setAlert] = useState(false);
  // const [minTgl, setMinTgl] = useState("");

  useEffect(() => {
    setAlert(false);
  }, [modalEdit]);

  const [input, setInput] = useState({
    bayar_bulog: "",
    tgl_bayar_bulog: "",
    terima_bayar: "",
    tgl_terima_bayar: "",
  });

  const handleChangeInput = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
    setAlert(false);
  };

  return (
    <Modal isOpen={modalEdit} fade={false} toggle={toggleEdit} size="md">
      <ModalHeader toggle={toggleEdit}>Ubah Rincian {mode}</ModalHeader>
      <ModalBody>
        {mode === "Terima" ? (
          <Card>
            <CardHeader>
              <strong>Terima Pembayaran</strong>
            </CardHeader>
            <CardBody>
              <Table bordered responsive>
                {data && (
                  <tbody>
                    <tr>
                      <td>Tagihan</td>
                      <td>
                        <label>
                          Rp
                          {formatNumber(stateRincian.payment?.tagihan_terima)}
                          ,-
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>Tanggal Terima</td>
                      <td>
                        <Input
                          name="tgl_terima_bayar"
                          type="date"
                          onChange={(e) => {
                            handleChange(e);
                            handleChangeInput(e);
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>Terima Senilai (Rp)</td>
                      <td>
                        <Input
                          name="terima_bayar"
                          type="number"
                          onChange={(e) => {
                            handleChange(e);
                            handleChangeInput(e);
                          }}
                        />
                      </td>
                    </tr>
                  </tbody>
                )}
              </Table>
            </CardBody>
          </Card>
        ) : mode === "Pembayaran" ? (
          <Card>
            <CardHeader>
              <strong>Bayar</strong>
            </CardHeader>
            <CardBody>
              <Table bordered responsive>
                {data && (
                  <tbody>
                    <tr>
                      <td>Tagihan</td>
                      <td>
                        <label>
                          Rp
                          {formatNumber(stateRincian.payment?.tagihan_bulog)}
                          ,-
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>Tanggal Bayar</td>
                      <td>
                        <Input
                          name="tgl_bayar_bulog"
                          type="date"
                          onChange={(e) => {
                            handleChange(e);
                            handleChangeInput(e);
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>Bayar Senilai (Rp)</td>
                      <td>
                        <Input
                          name="bayar_bulog"
                          type="number"
                          onChange={(e) => {
                            handleChange(e);
                            handleChangeInput(e);
                          }}
                        />
                      </td>
                    </tr>
                  </tbody>
                )}
              </Table>
            </CardBody>
          </Card>
        ) : null}
        <br />
        {alert ? (
          <Alert color="danger">Harap isi Form yang Kosong</Alert>
        ) : null}
      </ModalBody>
      <ModalFooter>
        {buttonLoading ? (
          <Button color="primary" outline>
            <Image src={loading} style={{ width: 20, height: 20 }} />
          </Button>
        ) : (
          <Button
            color="success"
            onClick={() => {
              if (mode === "Terima") {
                if (
                  input.terima_bayar === "" ||
                  input.tgl_terima_bayar === ""
                ) {
                  setAlert(true);
                } else {
                  handleSubmit("IN");
                  // console.log({ input });
                }
              } else if (mode === "Pembayaran") {
                if (input.bayar_bulog === "" || input.tgl_bayar_bulog === "") {
                  setAlert(true);
                } else {
                  handleSubmit("OUT");
                  // console.log({ input });
                }
              }
            }}
          >
            Selesai
          </Button>
        )}
      </ModalFooter>
    </Modal>
  );
};

export default EditPayment;
