import React from "react";

import { Image, Table } from "react-bootstrap";
import { formatNumber } from "../../utils";

import cancel from "../../assets/cancel.png";

const RincianBarang = ({ rincian, deleteRincian }) => {
  return (
    <>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th width="5%">#</th>
            <th width="20%">Nama Barang</th>
            <th width="10%">Unit</th>
            <th width="10%">Jumlah</th>
            <th width="20%">Harga Per Satuan</th>
            <th width="20%">HET</th>
            <th width="5%"></th>
          </tr>
        </thead>
        <tbody>
          {rincian.map((item, index) => (
            <tr>
              <td width="5%">{index + 1}</td>
              <td width="20%">{item.nama_barang}</td>
              <td width="10%">
                {item.unit} {item.tipe_unit}
              </td>
              <td width="10%">{item.jumlah}</td>
              <td width="20%">Rp{formatNumber(item.harga_perunit)}</td>
              <td width="20%">Rp{formatNumber(item.het)}</td>
              <td width="5%">
                <div style={{ justifyContent: "center", textAlign: "center" }}>
                  <Image
                    src={cancel}
                    style={{ cursor: "pointer", width: 30, height: 30 }}
                    onClick={() => {
                      deleteRincian(item.key);
                      // console.log("Hapus");
                    }}
                  />
                </div>
              </td>
            </tr>
          ))}{" "}
        </tbody>
      </Table>
    </>
  );
};

export default RincianBarang;