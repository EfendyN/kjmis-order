import axios from "axios";

export const API = axios.create({
  baseURL: "https://order-api-kjmis.lenturin.com/api/v1",
});

export const setHeaderAuth = (token) => {
  API.defaults.headers.common["Authorization"] = "Bearer " + token;
};
